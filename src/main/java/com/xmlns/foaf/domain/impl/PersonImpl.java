package com.xmlns.foaf.domain.impl;

import com.xmlns.foaf.domain.Person;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://xmlns.com/foaf/0.1/ ontology
 */
public class PersonImpl extends AgentImpl implements Person {
  public static final String TypeIRI = "http://xmlns.com/foaf/0.1/Person";

  protected PersonImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Person make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PersonImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Person.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Person.class,false);
          if(toRet == null) {
            toRet = new PersonImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Person)) {
          throw new RuntimeException("Instance of com.xmlns.foaf.domain.impl.PersonImpl expected");
        }
      }
      return (Person)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getPhone() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/phone",true);
  }

  public void setPhone(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/phone",val);
  }

  public String getSuffixName() {
    return this.getStringLit("http://purl.org/ontology/bibo/suffixName",true);
  }

  public void setSuffixName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/suffixName",val);
  }

  public String getGivenName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/givenName",true);
  }

  public void setGivenName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/givenName",val);
  }

  public String getSurName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/surName",true);
  }

  public void setSurName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/surName",val);
  }

  public String getPrefixName() {
    return this.getStringLit("http://purl.org/ontology/bibo/prefixName",true);
  }

  public void setPrefixName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/prefixName",val);
  }

  public String getOrcid() {
    return this.getExternalRef("http://gbol.life/0.1/orcid",true);
  }

  public void setOrcid(String val) {
    this.setExternalRef("http://gbol.life/0.1/orcid",val);
  }

  public String getDepiction() {
    return this.getExternalRef("http://xmlns.com/foaf/0.1/depiction",true);
  }

  public void setDepiction(String val) {
    this.setExternalRef("http://xmlns.com/foaf/0.1/depiction",val);
  }

  public String getFamilyName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/familyName",true);
  }

  public void setFamilyName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/familyName",val);
  }

  public String getLocalityName() {
    return this.getStringLit("http://purl.org/ontology/bibo/localityName",true);
  }

  public void setLocalityName(String val) {
    this.setStringLit("http://purl.org/ontology/bibo/localityName",val);
  }

  public String getTitle() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/title",val);
  }

  public String getFirstName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/firstName",true);
  }

  public void setFirstName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/firstName",val);
  }

  public String getHomepage() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/homepage",true);
  }

  public void setHomepage(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/homepage",val);
  }

  public String getMbox() {
    return this.getExternalRef("http://xmlns.com/foaf/0.1/mbox",true);
  }

  public void setMbox(String val) {
    this.setExternalRef("http://xmlns.com/foaf/0.1/mbox",val);
  }

  public String getName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/name",val);
  }
}
