package com.xmlns.foaf.domain;

import java.lang.String;

/**
 * Code generated from http://xmlns.com/foaf/0.1/ ontology
 */
public interface Person extends Agent {
  String getPhone();

  void setPhone(String val);

  String getSuffixName();

  void setSuffixName(String val);

  String getGivenName();

  void setGivenName(String val);

  String getSurName();

  void setSurName(String val);

  String getPrefixName();

  void setPrefixName(String val);

  String getOrcid();

  void setOrcid(String val);

  String getDepiction();

  void setDepiction(String val);

  String getFamilyName();

  void setFamilyName(String val);

  String getLocalityName();

  void setLocalityName(String val);

  String getTitle();

  void setTitle(String val);

  String getFirstName();

  void setFirstName(String val);

  String getHomepage();

  void setHomepage(String val);

  String getMbox();

  void setMbox(String val);
}
