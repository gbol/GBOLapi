package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Homology extends GenomicFeature {
  String getHomologousTo();

  void setHomologousTo(String val);
}
