package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Region extends Location {
  StrandPosition getStrand();

  void setStrand(StrandPosition val);

  Position getEnd();

  void setEnd(Position val);

  Position getBegin();

  void setBegin(Position val);
}
