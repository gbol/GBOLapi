package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum MobileElementType implements EnumClass {
  InsertionSequence("http://gbol.life/0.1/InsertionSequence",new MobileElementType[]{}),

  Transposon("http://gbol.life/0.1/Transposon",new MobileElementType[]{}),

  Retrotransposon("http://gbol.life/0.1/Retrotransposon",new MobileElementType[]{Transposon}),

  OtherMobileElementType("http://gbol.life/0.1/OtherMobileElementType",new MobileElementType[]{}),

  LINE("http://gbol.life/0.1/LINE",new MobileElementType[]{}),

  MITE("http://gbol.life/0.1/MITE",new MobileElementType[]{}),

  NonLTRRetrotransposon("http://gbol.life/0.1/NonLTRRetrotransposon",new MobileElementType[]{Retrotransposon}),

  Integron("http://gbol.life/0.1/Integron",new MobileElementType[]{}),

  SINE("http://gbol.life/0.1/SINE",new MobileElementType[]{});

  private MobileElementType[] parents;

  private String iri;

  private MobileElementType(String iri, MobileElementType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static MobileElementType make(String iri) {
    for(MobileElementType item : MobileElementType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
