package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface UnknownLengthAssemblyGap extends AssemblyGap {
  Integer getEstimatedLength();

  void setEstimatedLength(Integer val);
}
