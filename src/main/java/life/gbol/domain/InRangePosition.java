package life.gbol.domain;

import java.lang.Long;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface InRangePosition extends FuzzyPosition {
  Long getEndPosition();

  void setEndPosition(Long val);

  Long getBeginPosition();

  void setBeginPosition(Long val);
}
