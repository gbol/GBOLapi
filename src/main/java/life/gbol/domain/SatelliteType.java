package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum SatelliteType implements EnumClass {
  MicroSatellite("http://gbol.life/0.1/MicroSatellite",new SatelliteType[]{}),

  MiniSatellite("http://gbol.life/0.1/MiniSatellite",new SatelliteType[]{}),

  CommonSattellite("http://gbol.life/0.1/CommonSattellite",new SatelliteType[]{});

  private SatelliteType[] parents;

  private String iri;

  private SatelliteType(String iri, SatelliteType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static SatelliteType make(String iri) {
    for(SatelliteType item : SatelliteType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
