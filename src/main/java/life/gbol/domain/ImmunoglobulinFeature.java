package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ImmunoglobulinFeature extends ProteinFeature {
  ImmunoglobulinRegionType getImmunoglobulinRegionType();

  void setImmunoglobulinRegionType(ImmunoglobulinRegionType val);
}
