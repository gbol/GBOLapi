package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum FileType implements EnumClass {
  TXT("http://gbol.life/0.1/TXT",new FileType[]{}),

  GenBank("http://gbol.life/0.1/GenBank",new FileType[]{}),

  Other("http://gbol.life/0.1/Other",new FileType[]{}),

  TSV("http://gbol.life/0.1/TSV",new FileType[]{}),

  FASTA("http://gbol.life/0.1/FASTA",new FileType[]{}),

  FASTQ("http://gbol.life/0.1/FASTQ",new FileType[]{}),

  EMBL("http://gbol.life/0.1/EMBL",new FileType[]{}),

  CSV("http://gbol.life/0.1/CSV",new FileType[]{});

  private FileType[] parents;

  private String iri;

  private FileType(String iri, FileType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static FileType make(String iri) {
    for(FileType item : FileType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
