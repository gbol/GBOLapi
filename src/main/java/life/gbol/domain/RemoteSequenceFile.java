package life.gbol.domain;

import java.lang.Integer;
import java.lang.Long;
import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface RemoteSequenceFile extends RemoteDataFile {
  Long getReads();

  void setReads(Long val);

  SequencingPlatform getSequencer();

  void setSequencer(SequencingPlatform val);

  Long getBases();

  void setBases(Long val);

  Long getReadLength();

  void setReadLength(Long val);

  Integer getSequencingDepth();

  void setSequencingDepth(Integer val);

  ReadOrientation getStrandOrientation();

  void setStrandOrientation(ReadOrientation val);

  String getAdapter();

  void setAdapter(String val);
}
