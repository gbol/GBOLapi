package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProteinDomain extends ConservedRegion {
  void remSecondarySignature(String val);

  List<? extends String> getAllSecondarySignature();

  void addSecondarySignature(String val);

  String getSignatureDesc();

  void setSignatureDesc(String val);

  ProteinDomainType getDomainType();

  void setDomainType(ProteinDomainType val);

  String getSignature();

  void setSignature(String val);
}
