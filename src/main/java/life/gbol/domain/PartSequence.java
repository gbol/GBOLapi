package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PartSequence extends SequencePart {
  Region getIncludedRegion();

  void setIncludedRegion(Region val);
}
