package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Taxon extends OWLThing {
  Provenance getProvenance();

  void setProvenance(Provenance val);

  Taxon getSubClassOf();

  void setSubClassOf(Taxon val);

  String getTaxonName();

  void setTaxonName(String val);

  Rank getTaxonRank();

  void setTaxonRank(Rank val);
}
