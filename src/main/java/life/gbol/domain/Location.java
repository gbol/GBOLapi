package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Location extends OWLThing {
  Sequence getReferenceSequence();

  void setReferenceSequence(Sequence val);
}
