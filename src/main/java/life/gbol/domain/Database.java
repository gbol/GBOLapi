package life.gbol.domain;

import java.lang.String;
import java.time.LocalDateTime;
import org.rdfs.ns._void.domain.Dataset;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Database extends Entity, Dataset {
  LocalDateTime getReleaseDate();

  void setReleaseDate(LocalDateTime val);

  String getVersion();

  void setVersion(String val);

  String getId();

  void setId(String val);
}
