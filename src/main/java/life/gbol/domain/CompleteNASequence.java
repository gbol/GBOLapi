package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface CompleteNASequence extends NASequence {
  Organism getOrganism();

  void setOrganism(Organism val);

  StrandType getStrandType();

  void setStrandType(StrandType val);
}
