package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface BarcodeSet extends SequenceSet {
  Barcode getForwardBarcode();

  void setForwardBarcode(Barcode val);

  Barcode getReverseBarcode();

  void setReverseBarcode(Barcode val);
}
