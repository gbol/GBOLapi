package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Library extends OWLThing {
  Float getAccpetedSameBarcodeRatio();

  void setAccpetedSameBarcodeRatio(Float val);

  void remSample(Sample val);

  List<? extends Sample> getAllSample();

  void addSample(Sample val);

  Integer getLibraryNum();

  void setLibraryNum(Integer val);

  Integer getBarcodeHitsAccepted();

  void setBarcodeHitsAccepted(Integer val);

  String getFBarcodeFile();

  void setFBarcodeFile(String val);

  Float getBarcodeHitsAcceptedRatio();

  void setBarcodeHitsAcceptedRatio(Float val);

  Float getPrimerHitsAcceptedRatio();

  void setPrimerHitsAcceptedRatio(Float val);

  Provenance getProvenance();

  void setProvenance(Provenance val);

  String getRBarcodeFile();

  void setRBarcodeFile(String val);

  Integer getFBarcodeLength();

  void setFBarcodeLength(Integer val);

  String getRFile();

  void setRFile(String val);

  Integer getTotalReads();

  void setTotalReads(Integer val);

  Integer getPrimerHitsAccepted();

  void setPrimerHitsAccepted(Integer val);

  String getFFile();

  void setFFile(String val);

  Integer getRBarcodeLength();

  void setRBarcodeLength(Integer val);
}
