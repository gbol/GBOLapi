package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface VariantGenotype extends OWLThing {
  String getExpectedAlleleCounts();

  void setExpectedAlleleCounts(String val);

  void remGenotype(VariantGenotype val);

  List<? extends VariantGenotype> getAllGenotype();

  void addGenotype(VariantGenotype val);

  String getStrain();

  void setStrain(String val);

  Organism getAltOrganism();

  void setAltOrganism(Organism val);

  String getAltDepth();

  void setAltDepth(String val);

  Integer getPhredLikelihood();

  void setPhredLikelihood(Integer val);

  String getReadDepth();

  void setReadDepth(String val);

  String getMapQuality();

  void setMapQuality(String val);

  String getDepthQuality();

  void setDepthQuality(String val);

  String getGenotypeName();

  void setGenotypeName(String val);

  String getLikelihood();

  void setLikelihood(String val);

  String getGenotypeQuality();

  void setGenotypeQuality(String val);

  String getRefDepth();

  void setRefDepth(String val);

  String getHaploQuality();

  void setHaploQuality(String val);

  String getPhaseSet();

  void setPhaseSet(String val);

  String getPVal();

  void setPVal(String val);

  Integer getPhaseQuality();

  void setPhaseQuality(Integer val);

  String getFilter();

  void setFilter(String val);

  Float getPhredProb();

  void setPhredProb(Float val);

  String getFreq();

  void setFreq(String val);

  String getRealGenotype();

  void setRealGenotype(String val);

  Organism getRefOrganism();

  void setRefOrganism(Organism val);
}
