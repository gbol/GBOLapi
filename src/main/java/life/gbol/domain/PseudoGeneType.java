package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum PseudoGeneType implements EnumClass {
  Proccessed("http://gbol.life/0.1/Proccessed",new PseudoGeneType[]{}),

  Unprocessed("http://gbol.life/0.1/Unprocessed",new PseudoGeneType[]{}),

  Allelic("http://gbol.life/0.1/Allelic",new PseudoGeneType[]{}),

  PseudoGeneTypeUnknown("http://gbol.life/0.1/PseudoGeneTypeUnknown",new PseudoGeneType[]{}),

  Unitary("http://gbol.life/0.1/Unitary",new PseudoGeneType[]{});

  private PseudoGeneType[] parents;

  private String iri;

  private PseudoGeneType(String iri, PseudoGeneType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static PseudoGeneType make(String iri) {
    for(PseudoGeneType item : PseudoGeneType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
