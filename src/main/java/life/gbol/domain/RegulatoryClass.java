package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum RegulatoryClass implements EnumClass {
  PolyASignalSequence("http://gbol.life/0.1/PolyASignalSequence",new RegulatoryClass[]{}),

  Riboswitch("http://gbol.life/0.1/Riboswitch",new RegulatoryClass[]{}),

  RecodingStimulatoryRegion("http://gbol.life/0.1/RecodingStimulatoryRegion",new RegulatoryClass[]{}),

  Terminator("http://gbol.life/0.1/Terminator",new RegulatoryClass[]{}),

  ReplicationRegulatoryRegion("http://gbol.life/0.1/ReplicationRegulatoryRegion",new RegulatoryClass[]{}),

  Promoter("http://gbol.life/0.1/Promoter",new RegulatoryClass[]{}),

  GCSignal("http://gbol.life/0.1/GCSignal",new RegulatoryClass[]{}),

  Minus10Signal("http://gbol.life/0.1/Minus10Signal",new RegulatoryClass[]{}),

  Minus35Signal("http://gbol.life/0.1/Minus35Signal",new RegulatoryClass[]{}),

  DNaseIHypersensitiveSite("http://gbol.life/0.1/DNaseIHypersensitiveSite",new RegulatoryClass[]{}),

  Silencer("http://gbol.life/0.1/Silencer",new RegulatoryClass[]{}),

  LocusControlRegion("http://gbol.life/0.1/LocusControlRegion",new RegulatoryClass[]{}),

  CAATSignal("http://gbol.life/0.1/CAATSignal",new RegulatoryClass[]{}),

  ResponseElement("http://gbol.life/0.1/ResponseElement",new RegulatoryClass[]{}),

  RibosomeBindingSite("http://gbol.life/0.1/RibosomeBindingSite",new RegulatoryClass[]{}),

  Enhancer("http://gbol.life/0.1/Enhancer",new RegulatoryClass[]{}),

  TATABox("http://gbol.life/0.1/TATABox",new RegulatoryClass[]{}),

  TranscriptionalCisRegulatoryRegion("http://gbol.life/0.1/TranscriptionalCisRegulatoryRegion",new RegulatoryClass[]{}),

  ImprintingControlRegion("http://gbol.life/0.1/ImprintingControlRegion",new RegulatoryClass[]{}),

  Minus12Signal("http://gbol.life/0.1/Minus12Signal",new RegulatoryClass[]{}),

  Insulator("http://gbol.life/0.1/Insulator",new RegulatoryClass[]{}),

  MatrixAttachmentRegion("http://gbol.life/0.1/MatrixAttachmentRegion",new RegulatoryClass[]{}),

  Attenuator("http://gbol.life/0.1/Attenuator",new RegulatoryClass[]{}),

  EnhancerBlockingElement("http://gbol.life/0.1/EnhancerBlockingElement",new RegulatoryClass[]{}),

  OtherRegulation("http://gbol.life/0.1/OtherRegulation",new RegulatoryClass[]{});

  private RegulatoryClass[] parents;

  private String iri;

  private RegulatoryClass(String iri, RegulatoryClass[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static RegulatoryClass make(String iri) {
    for(RegulatoryClass item : RegulatoryClass.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
