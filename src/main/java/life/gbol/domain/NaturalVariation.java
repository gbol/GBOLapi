package life.gbol.domain;

import java.lang.Double;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface NaturalVariation extends VariationFeature {
  Double getFrequency();

  void setFrequency(Double val);
}
