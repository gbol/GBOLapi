package life.gbol.domain;

import java.lang.String;
import org.w3.ns.prov.domain.SoftwareAgent;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AnnotationSoftware extends SoftwareAgent {
  String getName();

  void setName(String val);

  String getCommitId();

  void setCommitId(String val);

  String getVersion();

  void setVersion(String val);

  String getCodeRepository();

  void setCodeRepository(String val);

  String getHomepage();

  void setHomepage(String val);
}
