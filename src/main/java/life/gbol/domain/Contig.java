package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Contig extends UncompleteNASequence {
  void remSourceReads(Read val);

  List<? extends Read> getAllSourceReads();

  void addSourceReads(Read val);
}
