package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ManualAnnotationActivity extends AnnotationActivity {
  Curator getWasAssociatedWith();

  void setWasAssociatedWith(Curator val);
}
