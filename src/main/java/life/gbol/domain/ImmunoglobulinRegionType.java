package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ImmunoglobulinRegionType implements EnumClass {
  ConstantRegion("http://gbol.life/0.1/ConstantRegion",new ImmunoglobulinRegionType[]{}),

  SwitchRegion("http://gbol.life/0.1/SwitchRegion",new ImmunoglobulinRegionType[]{}),

  GapRegion("http://gbol.life/0.1/GapRegion",new ImmunoglobulinRegionType[]{}),

  DiversitySegment("http://gbol.life/0.1/DiversitySegment",new ImmunoglobulinRegionType[]{}),

  JoiningSegment("http://gbol.life/0.1/JoiningSegment",new ImmunoglobulinRegionType[]{}),

  VariableRegion("http://gbol.life/0.1/VariableRegion",new ImmunoglobulinRegionType[]{}),

  VariableSegment("http://gbol.life/0.1/VariableSegment",new ImmunoglobulinRegionType[]{});

  private ImmunoglobulinRegionType[] parents;

  private String iri;

  private ImmunoglobulinRegionType(String iri, ImmunoglobulinRegionType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ImmunoglobulinRegionType make(String iri) {
    for(ImmunoglobulinRegionType item : ImmunoglobulinRegionType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
