package life.gbol.domain;

import java.lang.Long;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface OneOfPosition extends FuzzyPosition {
  void remPosition(Long val);

  List<? extends Long> getAllPosition();

  void addPosition(Long val);
}
