package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum BaseType implements EnumClass {
  BaseM3c("http://gbol.life/0.1/BaseM3c",new BaseType[]{}),

  BaseM7g("http://gbol.life/0.1/BaseM7g",new BaseType[]{}),

  BaseMs2i6a("http://gbol.life/0.1/BaseMs2i6a",new BaseType[]{}),

  BaseMv("http://gbol.life/0.1/BaseMv",new BaseType[]{}),

  BaseMan_q("http://gbol.life/0.1/BaseMan_q",new BaseType[]{}),

  BaseX("http://gbol.life/0.1/BaseX",new BaseType[]{}),

  BaseT("http://gbol.life/0.1/BaseT",new BaseType[]{}),

  BaseDhu("http://gbol.life/0.1/BaseDhu",new BaseType[]{}),

  BaseYw("http://gbol.life/0.1/BaseYw",new BaseType[]{}),

  BaseP("http://gbol.life/0.1/BaseP",new BaseType[]{}),

  BaseUm("http://gbol.life/0.1/BaseUm",new BaseType[]{}),

  BaseQ("http://gbol.life/0.1/BaseQ",new BaseType[]{}),

  BaseM5c("http://gbol.life/0.1/BaseM5c",new BaseType[]{}),

  BaseS4u("http://gbol.life/0.1/BaseS4u",new BaseType[]{}),

  BaseMam5u("http://gbol.life/0.1/BaseMam5u",new BaseType[]{}),

  BaseGal_q("http://gbol.life/0.1/BaseGal_q",new BaseType[]{}),

  BaseI("http://gbol.life/0.1/BaseI",new BaseType[]{}),

  BaseM4c("http://gbol.life/0.1/BaseM4c",new BaseType[]{}),

  BaseT6a("http://gbol.life/0.1/BaseT6a",new BaseType[]{}),

  BaseMcm5s2u("http://gbol.life/0.1/BaseMcm5s2u",new BaseType[]{}),

  BaseM22g("http://gbol.life/0.1/BaseM22g",new BaseType[]{}),

  BaseM5u("http://gbol.life/0.1/BaseM5u",new BaseType[]{}),

  BaseO5u("http://gbol.life/0.1/BaseO5u",new BaseType[]{}),

  BaseChm5u("http://gbol.life/0.1/BaseChm5u",new BaseType[]{}),

  BaseTm("http://gbol.life/0.1/BaseTm",new BaseType[]{}),

  BaseI6a("http://gbol.life/0.1/BaseI6a",new BaseType[]{}),

  BaseCmnm5u("http://gbol.life/0.1/BaseCmnm5u",new BaseType[]{}),

  BaseM6a("http://gbol.life/0.1/BaseM6a",new BaseType[]{}),

  BaseM1a("http://gbol.life/0.1/BaseM1a",new BaseType[]{}),

  BaseOsyw("http://gbol.life/0.1/BaseOsyw",new BaseType[]{}),

  BaseM1g("http://gbol.life/0.1/BaseM1g",new BaseType[]{}),

  BaseM1f("http://gbol.life/0.1/BaseM1f",new BaseType[]{}),

  BaseMo5u("http://gbol.life/0.1/BaseMo5u",new BaseType[]{}),

  BaseCm("http://gbol.life/0.1/BaseCm",new BaseType[]{}),

  BaseM1i("http://gbol.life/0.1/BaseM1i",new BaseType[]{}),

  BaseMs2t6a("http://gbol.life/0.1/BaseMs2t6a",new BaseType[]{}),

  BaseGm("http://gbol.life/0.1/BaseGm",new BaseType[]{}),

  BaseMt6a("http://gbol.life/0.1/BaseMt6a",new BaseType[]{}),

  BaseMam5s2u("http://gbol.life/0.1/BaseMam5s2u",new BaseType[]{}),

  BaseOther("http://gbol.life/0.1/BaseOther",new BaseType[]{}),

  BaseAc4c("http://gbol.life/0.1/BaseAc4c",new BaseType[]{}),

  BaseS2t("http://gbol.life/0.1/BaseS2t",new BaseType[]{}),

  BaseS2u("http://gbol.life/0.1/BaseS2u",new BaseType[]{}),

  BaseM2a("http://gbol.life/0.1/BaseM2a",new BaseType[]{}),

  BaseM2g("http://gbol.life/0.1/BaseM2g",new BaseType[]{}),

  BaseMcm5u("http://gbol.life/0.1/BaseMcm5u",new BaseType[]{}),

  BaseS2c("http://gbol.life/0.1/BaseS2c",new BaseType[]{}),

  BaseCmnm5s2u("http://gbol.life/0.1/BaseCmnm5s2u",new BaseType[]{}),

  BaseFm("http://gbol.life/0.1/BaseFm",new BaseType[]{});

  private BaseType[] parents;

  private String iri;

  private BaseType(String iri, BaseType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static BaseType make(String iri) {
    for(BaseType item : BaseType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
