package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface QTLMap extends OWLThing {
  String getMapAccession();

  void setMapAccession(String val);

  MapUnits getUnits();

  void setUnits(MapUnits val);

  String getMapName();

  void setMapName(String val);

  void remParent(Organism val);

  List<? extends Organism> getAllParent();

  void addParent(Organism val);
}
