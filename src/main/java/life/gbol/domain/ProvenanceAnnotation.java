package life.gbol.domain;

import java.lang.String;
import java.util.List;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProvenanceAnnotation extends ProvenanceApplication {
  Document getReference();

  void setReference(Document val);

  void remDerivedFrom(Feature val);

  List<? extends Feature> getAllDerivedFrom();

  void addDerivedFrom(Feature val);

  String getExperimentalEvidence();

  void setExperimentalEvidence(String val);

  String getProvenanceNote();

  void setProvenanceNote(String val);
}
