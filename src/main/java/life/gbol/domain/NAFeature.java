package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface NAFeature extends Feature {
  void remExpression(Expression val);

  List<? extends Expression> getAllExpression();

  void addExpression(Expression val);

  String getMapLocation();

  void setMapLocation(String val);
}
