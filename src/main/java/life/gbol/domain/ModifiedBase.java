package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ModifiedBase extends SequenceAnnotation {
  BaseType getModBase();

  void setModBase(BaseType val);

  String getModificationFunction();

  void setModificationFunction(String val);
}
