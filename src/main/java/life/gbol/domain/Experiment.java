package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Experiment extends OWLThing {
  ReadInformation getInputReads();

  void setInputReads(ReadInformation val);

  Provenance getProvenance();

  void setProvenance(Provenance val);
}
