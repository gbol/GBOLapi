package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Provenance extends OWLThing {
  AnnotationResult getOrigin();

  void setOrigin(AnnotationResult val);

  ProvenanceApplication getAnnotation();

  void setAnnotation(ProvenanceApplication val);
}
