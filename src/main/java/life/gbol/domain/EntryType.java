package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum EntryType implements EnumClass {
  HighLevelAssemblyInformation("http://gbol.life/0.1/HighLevelAssemblyInformation",new EntryType[]{}),

  MassGenomeAnnotationEntry("http://gbol.life/0.1/MassGenomeAnnotationEntry",new EntryType[]{}),

  GenomeSurveySequenceEntry("http://gbol.life/0.1/GenomeSurveySequenceEntry",new EntryType[]{}),

  StandardEntry("http://gbol.life/0.1/StandardEntry",new EntryType[]{}),

  SequenceTaggedSiteEntry("http://gbol.life/0.1/SequenceTaggedSiteEntry",new EntryType[]{}),

  HighThoughputCDNASequencingEntry("http://gbol.life/0.1/HighThoughputCDNASequencingEntry",new EntryType[]{}),

  WholeGenomeShotgunEntry("http://gbol.life/0.1/WholeGenomeShotgunEntry",new EntryType[]{}),

  TranscriptomeShotgunAssemblyEntry("http://gbol.life/0.1/TranscriptomeShotgunAssemblyEntry",new EntryType[]{}),

  EnvironmentalSamplingSequencesEntry("http://gbol.life/0.1/EnvironmentalSamplingSequencesEntry",new EntryType[]{}),

  ExpressedSequenceTagEntry("http://gbol.life/0.1/ExpressedSequenceTagEntry",new EntryType[]{}),

  HighThoughputGenomeSequencingEntry("http://gbol.life/0.1/HighThoughputGenomeSequencingEntry",new EntryType[]{}),

  PatentEntry("http://gbol.life/0.1/PatentEntry",new EntryType[]{}),

  ThirdPArtySequenceDataRecord("http://gbol.life/0.1/ThirdPArtySequenceDataRecord",new EntryType[]{});

  private EntryType[] parents;

  private String iri;

  private EntryType(String iri, EntryType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static EntryType make(String iri) {
    for(EntryType item : EntryType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
