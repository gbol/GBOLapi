package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface TaxonomyRef extends XRef {
  NomenclaturalType getNomenclaturalType();

  void setNomenclaturalType(NomenclaturalType val);
}
