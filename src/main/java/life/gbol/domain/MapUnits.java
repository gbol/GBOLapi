package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum MapUnits implements EnumClass {
  bp("http://gbol.life/0.1/bp",new MapUnits[]{}),

  cM("http://gbol.life/0.1/cM",new MapUnits[]{});

  private MapUnits[] parents;

  private String iri;

  private MapUnits(String iri, MapUnits[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static MapUnits make(String iri) {
    for(MapUnits item : MapUnits.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
