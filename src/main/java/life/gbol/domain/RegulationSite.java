package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface RegulationSite extends BiologicalRecognizedRegion {
  RegulatoryClass getRegulatoryClass();

  void setRegulatoryClass(RegulatoryClass val);
}
