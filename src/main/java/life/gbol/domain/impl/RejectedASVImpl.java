package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.ASVAssignment;
import life.gbol.domain.ASVSequence;
import life.gbol.domain.RejectedASV;
import life.gbol.domain.Taxon;
import life.gbol.domain.TaxonAssignmentType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class RejectedASVImpl extends ASVSetImpl implements RejectedASV {
  public static final String TypeIRI = "http://gbol.life/0.1/RejectedASV";

  protected RejectedASVImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static RejectedASV make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new RejectedASVImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,RejectedASV.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,RejectedASV.class,false);
          if(toRet == null) {
            toRet = new RejectedASVImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof RejectedASV)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.RejectedASVImpl expected");
        }
      }
      return (RejectedASV)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getMasterASVId() {
    return this.getStringLit("http://gbol.life/0.1/masterASVId",false);
  }

  public void setMasterASVId(String val) {
    this.setStringLit("http://gbol.life/0.1/masterASVId",val);
  }

  public Float getRatio() {
    return this.getFloatLit("http://gbol.life/0.1/ratio",true);
  }

  public void setRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/ratio",val);
  }

  public Integer getClusteredReadCount() {
    return this.getIntegerLit("http://gbol.life/0.1/clusteredReadCount",true);
  }

  public void setClusteredReadCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/clusteredReadCount",val);
  }

  public ASVSequence getReverseASV() {
    return this.getRef("http://gbol.life/0.1/reverseASV",true,ASVSequence.class);
  }

  public void setReverseASV(ASVSequence val) {
    this.setRef("http://gbol.life/0.1/reverseASV",val,ASVSequence.class);
  }

  public ASVSequence getForwardASV() {
    return this.getRef("http://gbol.life/0.1/forwardASV",false,ASVSequence.class);
  }

  public void setForwardASV(ASVSequence val) {
    this.setRef("http://gbol.life/0.1/forwardASV",val,ASVSequence.class);
  }

  public Taxon getAssignedTaxon() {
    return this.getRef("http://gbol.life/0.1/assignedTaxon",true,Taxon.class);
  }

  public void setAssignedTaxon(Taxon val) {
    this.setRef("http://gbol.life/0.1/assignedTaxon",val,Taxon.class);
  }

  public Integer getReadCount() {
    return this.getIntegerLit("http://gbol.life/0.1/readCount",true);
  }

  public void setReadCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/readCount",val);
  }

  public void remAsvAssignment(ASVAssignment val) {
    this.remRef("http://gbol.life/0.1/asvAssignment",val,true);
  }

  public List<? extends ASVAssignment> getAllAsvAssignment() {
    return this.getRefSet("http://gbol.life/0.1/asvAssignment",true,ASVAssignment.class);
  }

  public void addAsvAssignment(ASVAssignment val) {
    this.addRef("http://gbol.life/0.1/asvAssignment",val);
  }

  public Integer getTaxonHitCount() {
    return this.getIntegerLit("http://gbol.life/0.1/taxonHitCount",true);
  }

  public void setTaxonHitCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/taxonHitCount",val);
  }

  public void remMismatchLevelCount(Integer val) {
    this.remIntegerLit("http://gbol.life/0.1/mismatchLevelCount",val,true);
  }

  public List<? extends Integer> getAllMismatchLevelCount() {
    return this.getIntegerLitSet("http://gbol.life/0.1/mismatchLevelCount",true);
  }

  public void addMismatchLevelCount(Integer val) {
    this.addIntegerLit("http://gbol.life/0.1/mismatchLevelCount",val);
  }

  public Integer getUsedTaxonLevel() {
    return this.getIntegerLit("http://gbol.life/0.1/usedTaxonLevel",true);
  }

  public void setUsedTaxonLevel(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/usedTaxonLevel",val);
  }

  public Integer getMismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/mismatchCount",true);
  }

  public void setMismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/mismatchCount",val);
  }

  public TaxonAssignmentType getTaxonType() {
    return this.getEnum("http://gbol.life/0.1/taxonType",true,TaxonAssignmentType.class);
  }

  public void setTaxonType(TaxonAssignmentType val) {
    this.setEnum("http://gbol.life/0.1/taxonType",val,TaxonAssignmentType.class);
  }
}
