package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProvenanceApplication;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceApplicationImpl extends OWLThingImpl implements ProvenanceApplication {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceApplication";

  protected ProvenanceApplicationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceApplication make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceApplicationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceApplication.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceApplication.class,false);
          if(toRet == null) {
            toRet = new ProvenanceApplicationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceApplication)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceApplicationImpl expected");
        }
      }
      return (ProvenanceApplication)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
