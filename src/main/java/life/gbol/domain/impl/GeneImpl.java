package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.Exon;
import life.gbol.domain.Expression;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Gene;
import life.gbol.domain.Intron;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.Operon;
import life.gbol.domain.PseudoGeneType;
import life.gbol.domain.ReasonArtificialLocation;
import life.gbol.domain.Transcript;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class GeneImpl extends TranscriptionElementImpl implements Gene {
  public static final String TypeIRI = "http://gbol.life/0.1/Gene";

  protected GeneImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Gene make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new GeneImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Gene.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Gene.class,false);
          if(toRet == null) {
            toRet = new GeneImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Gene)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.GeneImpl expected");
        }
      }
      return (Gene)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/locusTag");
  }

  public void remGeneSymbolSynonym(String val) {
    this.remStringLit("http://gbol.life/0.1/geneSymbolSynonym",val,true);
  }

  public List<? extends String> getAllGeneSymbolSynonym() {
    return this.getStringLitSet("http://gbol.life/0.1/geneSymbolSynonym",true);
  }

  public void addGeneSymbolSynonym(String val) {
    this.addStringLit("http://gbol.life/0.1/geneSymbolSynonym",val);
  }

  public void remExon(Exon val) {
    this.remRef("http://gbol.life/0.1/exon",val,true);
  }

  public List<? extends Exon> getAllExon() {
    return this.getRefSet("http://gbol.life/0.1/exon",true,Exon.class);
  }

  public void addExon(Exon val) {
    this.addRef("http://gbol.life/0.1/exon",val);
  }

  public void remAllele(String val) {
    this.remStringLit("http://gbol.life/0.1/allele",val,true);
  }

  public List<? extends String> getAllAllele() {
    return this.getStringLitSet("http://gbol.life/0.1/allele",true);
  }

  public void addAllele(String val) {
    this.addStringLit("http://gbol.life/0.1/allele",val);
  }

  public PseudoGeneType getPseudogene() {
    return this.getEnum("http://gbol.life/0.1/pseudogene",true,PseudoGeneType.class);
  }

  public void setPseudogene(PseudoGeneType val) {
    this.setEnum("http://gbol.life/0.1/pseudogene",val,PseudoGeneType.class);
  }

  public String getOldLocusTag() {
    return this.getStringLit("http://gbol.life/0.1/oldLocusTag",true);
  }

  public void setOldLocusTag(String val) {
    this.setStringLit("http://gbol.life/0.1/oldLocusTag",val);
  }

  public void remIntron(Intron val) {
    this.remRef("http://gbol.life/0.1/intron",val,true);
  }

  public List<? extends Intron> getAllIntron() {
    return this.getRefSet("http://gbol.life/0.1/intron",true,Intron.class);
  }

  public void addIntron(Intron val) {
    this.addRef("http://gbol.life/0.1/intron",val);
  }

  public void remTranscript(Transcript val) {
    this.remRef("http://gbol.life/0.1/transcript",val,true);
  }

  public List<? extends Transcript> getAllTranscript() {
    return this.getRefSet("http://gbol.life/0.1/transcript",true,Transcript.class);
  }

  public void addTranscript(Transcript val) {
    this.addRef("http://gbol.life/0.1/transcript",val);
  }

  public String getLocusTag() {
    return this.getStringLit("http://gbol.life/0.1/locusTag",false);
  }

  public void setLocusTag(String val) {
    this.setStringLit("http://gbol.life/0.1/locusTag",val);
  }

  public String getGeneSymbol() {
    return this.getStringLit("http://gbol.life/0.1/geneSymbol",true);
  }

  public void setGeneSymbol(String val) {
    this.setStringLit("http://gbol.life/0.1/geneSymbol",val);
  }

  public Operon getOperon() {
    return this.getRef("http://gbol.life/0.1/operon",true,Operon.class);
  }

  public void setOperon(Operon val) {
    this.setRef("http://gbol.life/0.1/operon",val,Operon.class);
  }

  public void remExpression(Expression val) {
    this.remRef("http://gbol.life/0.1/expression",val,true);
  }

  public List<? extends Expression> getAllExpression() {
    return this.getRefSet("http://gbol.life/0.1/expression",true,Expression.class);
  }

  public void addExpression(Expression val) {
    this.addRef("http://gbol.life/0.1/expression",val);
  }

  public String getMapLocation() {
    return this.getStringLit("http://gbol.life/0.1/mapLocation",true);
  }

  public void setMapLocation(String val) {
    this.setStringLit("http://gbol.life/0.1/mapLocation",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public void remProvenance(FeatureProvenance val) {
    this.remRef("http://gbol.life/0.1/provenance",val,false);
  }

  public List<? extends FeatureProvenance> getAllProvenance() {
    return this.getRefSet("http://gbol.life/0.1/provenance",false,FeatureProvenance.class);
  }

  public void addProvenance(FeatureProvenance val) {
    this.addRef("http://gbol.life/0.1/provenance",val);
  }

  public ReasonArtificialLocation getArtificialLocation() {
    return this.getEnum("http://gbol.life/0.1/artificialLocation",true,ReasonArtificialLocation.class);
  }

  public void setArtificialLocation(ReasonArtificialLocation val) {
    this.setEnum("http://gbol.life/0.1/artificialLocation",val,ReasonArtificialLocation.class);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
