package life.gbol.domain.impl;

import com.xmlns.foaf.domain.Agent;
import java.lang.Boolean;
import java.lang.Double;
import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.ASVSet;
import life.gbol.domain.BarcodeSet;
import life.gbol.domain.Citation;
import life.gbol.domain.GeographicalCoordinate;
import life.gbol.domain.GeographicalLocation;
import life.gbol.domain.MaterialSource;
import life.gbol.domain.Note;
import life.gbol.domain.PCRPrimerSet;
import life.gbol.domain.Sample;
import life.gbol.domain.TaxonomyRef;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SampleImpl extends OWLThingImpl implements Sample {
  public static final String TypeIRI = "http://gbol.life/0.1/Sample";

  protected SampleImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Sample make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SampleImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Sample.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Sample.class,false);
          if(toRet == null) {
            toRet = new SampleImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Sample)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SampleImpl expected");
        }
      }
      return (Sample)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/name");
  }

  public Boolean getProviralExtraction() {
    return this.getBooleanLit("http://gbol.life/0.1/proviralExtraction",true);
  }

  public void setProviralExtraction(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/proviralExtraction",val);
  }

  public String getMap() {
    return this.getStringLit("http://gbol.life/0.1/map",true);
  }

  public void setMap(String val) {
    this.setStringLit("http://gbol.life/0.1/map",val);
  }

  public void remMetadata(String val) {
    this.remExternalRef("http://gbol.life/0.1/metadata",val,true);
  }

  public List<? extends String> getAllMetadata() {
    return this.getExternalRefSet("http://gbol.life/0.1/metadata",true);
  }

  public void addMetadata(String val) {
    this.addExternalRef("http://gbol.life/0.1/metadata",val);
  }

  public String getStrain() {
    return this.getStringLit("http://gbol.life/0.1/strain",true);
  }

  public void setStrain(String val) {
    this.setStringLit("http://gbol.life/0.1/strain",val);
  }

  public String getEcotype() {
    return this.getStringLit("http://gbol.life/0.1/ecotype",true);
  }

  public void setEcotype(String val) {
    this.setStringLit("http://gbol.life/0.1/ecotype",val);
  }

  public String getSubClone() {
    return this.getStringLit("http://gbol.life/0.1/subClone",true);
  }

  public void setSubClone(String val) {
    this.setStringLit("http://gbol.life/0.1/subClone",val);
  }

  public String getIsolate() {
    return this.getStringLit("http://gbol.life/0.1/isolate",true);
  }

  public void setIsolate(String val) {
    this.setStringLit("http://gbol.life/0.1/isolate",val);
  }

  public String getHaplogroup() {
    return this.getStringLit("http://gbol.life/0.1/haplogroup",true);
  }

  public void setHaplogroup(String val) {
    this.setStringLit("http://gbol.life/0.1/haplogroup",val);
  }

  public String getDevStage() {
    return this.getStringLit("http://gbol.life/0.1/devStage",true);
  }

  public void setDevStage(String val) {
    this.setStringLit("http://gbol.life/0.1/devStage",val);
  }

  public String getSex() {
    return this.getStringLit("http://gbol.life/0.1/sex",true);
  }

  public void setSex(String val) {
    this.setStringLit("http://gbol.life/0.1/sex",val);
  }

  public String getPlasmid() {
    return this.getStringLit("http://gbol.life/0.1/plasmid",true);
  }

  public void setPlasmid(String val) {
    this.setStringLit("http://gbol.life/0.1/plasmid",val);
  }

  public BarcodeSet getBarcodes() {
    return this.getRef("http://gbol.life/0.1/barcodes",true,BarcodeSet.class);
  }

  public void setBarcodes(BarcodeSet val) {
    this.setRef("http://gbol.life/0.1/barcodes",val,BarcodeSet.class);
  }

  public String getPopVariant() {
    return this.getStringLit("http://gbol.life/0.1/popVariant",true);
  }

  public void setPopVariant(String val) {
    this.setStringLit("http://gbol.life/0.1/popVariant",val);
  }

  public String getHaplotype() {
    return this.getStringLit("http://gbol.life/0.1/haplotype",true);
  }

  public void setHaplotype(String val) {
    this.setStringLit("http://gbol.life/0.1/haplotype",val);
  }

  public String getName() {
    return this.getStringLit("http://gbol.life/0.1/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://gbol.life/0.1/name",val);
  }

  public String getIsolationSource() {
    return this.getStringLit("http://gbol.life/0.1/isolationSource",true);
  }

  public void setIsolationSource(String val) {
    this.setStringLit("http://gbol.life/0.1/isolationSource",val);
  }

  public String getLabHost() {
    return this.getStringLit("http://gbol.life/0.1/labHost",true);
  }

  public void setLabHost(String val) {
    this.setStringLit("http://gbol.life/0.1/labHost",val);
  }

  public GeographicalCoordinate getGeographicalCoordinate() {
    return this.getRef("http://gbol.life/0.1/geographicalCoordinate",true,GeographicalCoordinate.class);
  }

  public void setGeographicalCoordinate(GeographicalCoordinate val) {
    this.setRef("http://gbol.life/0.1/geographicalCoordinate",val,GeographicalCoordinate.class);
  }

  public String getTissueLib() {
    return this.getStringLit("http://gbol.life/0.1/tissueLib",true);
  }

  public void setTissueLib(String val) {
    this.setStringLit("http://gbol.life/0.1/tissueLib",val);
  }

  public String getOrganelle() {
    return this.getExternalRef("http://gbol.life/0.1/organelle",true);
  }

  public void setOrganelle(String val) {
    this.setExternalRef("http://gbol.life/0.1/organelle",val);
  }

  public String getCellType() {
    return this.getStringLit("http://gbol.life/0.1/cellType",true);
  }

  public void setCellType(String val) {
    this.setStringLit("http://gbol.life/0.1/cellType",val);
  }

  public String getSubSpecies() {
    return this.getStringLit("http://gbol.life/0.1/subSpecies",true);
  }

  public void setSubSpecies(String val) {
    this.setStringLit("http://gbol.life/0.1/subSpecies",val);
  }

  public Double getAltitude() {
    return this.getDoubleLit("http://gbol.life/0.1/altitude",true);
  }

  public void setAltitude(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/altitude",val);
  }

  public String getCellLine() {
    return this.getStringLit("http://gbol.life/0.1/cellLine",true);
  }

  public void setCellLine(String val) {
    this.setStringLit("http://gbol.life/0.1/cellLine",val);
  }

  public String getTissueType() {
    return this.getStringLit("http://gbol.life/0.1/tissueType",true);
  }

  public void setTissueType(String val) {
    this.setStringLit("http://gbol.life/0.1/tissueType",val);
  }

  public String getCultivar() {
    return this.getStringLit("http://gbol.life/0.1/cultivar",true);
  }

  public void setCultivar(String val) {
    this.setStringLit("http://gbol.life/0.1/cultivar",val);
  }

  public String getMatingType() {
    return this.getStringLit("http://gbol.life/0.1/matingType",true);
  }

  public void setMatingType(String val) {
    this.setStringLit("http://gbol.life/0.1/matingType",val);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public void remSourceMaterial(MaterialSource val) {
    this.remRef("http://gbol.life/0.1/sourceMaterial",val,true);
  }

  public List<? extends MaterialSource> getAllSourceMaterial() {
    return this.getRefSet("http://gbol.life/0.1/sourceMaterial",true,MaterialSource.class);
  }

  public void addSourceMaterial(MaterialSource val) {
    this.addRef("http://gbol.life/0.1/sourceMaterial",val);
  }

  public String getSubStrain() {
    return this.getStringLit("http://gbol.life/0.1/subStrain",true);
  }

  public void setSubStrain(String val) {
    this.setStringLit("http://gbol.life/0.1/subStrain",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life/0.1/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life/0.1/description",val);
  }

  public String getChromosome() {
    return this.getStringLit("http://gbol.life/0.1/chromosome",true);
  }

  public void setChromosome(String val) {
    this.setStringLit("http://gbol.life/0.1/chromosome",val);
  }

  public void remCollectedBy(Agent val) {
    this.remRef("http://gbol.life/0.1/collectedBy",val,true);
  }

  public List<? extends Agent> getAllCollectedBy() {
    return this.getRefSet("http://gbol.life/0.1/collectedBy",true,Agent.class);
  }

  public void addCollectedBy(Agent val) {
    this.addRef("http://gbol.life/0.1/collectedBy",val);
  }

  public Agent getIdentifiedBy() {
    return this.getRef("http://gbol.life/0.1/identifiedBy",true,Agent.class);
  }

  public void setIdentifiedBy(Agent val) {
    this.setRef("http://gbol.life/0.1/identifiedBy",val,Agent.class);
  }

  public void remAsv(ASVSet val) {
    this.remRef("http://gbol.life/0.1/asv",val,true);
  }

  public List<? extends ASVSet> getAllAsv() {
    return this.getRefSet("http://gbol.life/0.1/asv",true,ASVSet.class);
  }

  public void addAsv(ASVSet val) {
    this.addRef("http://gbol.life/0.1/asv",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public String getVariety() {
    return this.getStringLit("http://gbol.life/0.1/variety",true);
  }

  public void setVariety(String val) {
    this.setStringLit("http://gbol.life/0.1/variety",val);
  }

  public TaxonomyRef getHost() {
    return this.getRef("http://gbol.life/0.1/host",true,TaxonomyRef.class);
  }

  public void setHost(TaxonomyRef val) {
    this.setRef("http://gbol.life/0.1/host",val,TaxonomyRef.class);
  }

  public PCRPrimerSet getPCRPrimers() {
    return this.getRef("http://gbol.life/0.1/PCRPrimers",true,PCRPrimerSet.class);
  }

  public void setPCRPrimers(PCRPrimerSet val) {
    this.setRef("http://gbol.life/0.1/PCRPrimers",val,PCRPrimerSet.class);
  }

  public LocalDateTime getCollectionDate() {
    return this.getDateTimeLit("http://gbol.life/0.1/collectionDate",true);
  }

  public void setCollectionDate(LocalDateTime val) {
    this.setDateTimeLit("http://gbol.life/0.1/collectionDate",val);
  }

  public GeographicalLocation getGeographicalLocation() {
    return this.getRef("http://gbol.life/0.1/geographicalLocation",true,GeographicalLocation.class);
  }

  public void setGeographicalLocation(GeographicalLocation val) {
    this.setRef("http://gbol.life/0.1/geographicalLocation",val,GeographicalLocation.class);
  }

  public String getSegment() {
    return this.getStringLit("http://gbol.life/0.1/segment",true);
  }

  public void setSegment(String val) {
    this.setStringLit("http://gbol.life/0.1/segment",val);
  }

  public Boolean getEnvironmentalSample() {
    return this.getBooleanLit("http://gbol.life/0.1/environmentalSample",true);
  }

  public void setEnvironmentalSample(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/environmentalSample",val);
  }

  public String getSerotype() {
    return this.getStringLit("http://gbol.life/0.1/serotype",true);
  }

  public void setSerotype(String val) {
    this.setStringLit("http://gbol.life/0.1/serotype",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
