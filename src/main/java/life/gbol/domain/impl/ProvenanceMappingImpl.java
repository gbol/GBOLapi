package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProvenanceMapping;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceMappingImpl extends ProvenanceApplicationImpl implements ProvenanceMapping {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceMapping";

  protected ProvenanceMappingImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceMapping make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceMappingImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceMapping.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceMapping.class,false);
          if(toRet == null) {
            toRet = new ProvenanceMappingImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceMapping)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceMappingImpl expected");
        }
      }
      return (ProvenanceMapping)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
