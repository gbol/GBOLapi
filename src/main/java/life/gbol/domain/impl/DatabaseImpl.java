package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.Database;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class DatabaseImpl extends EntityImpl implements Database {
  public static final String TypeIRI = "http://gbol.life/0.1/Database";

  protected DatabaseImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Database make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatabaseImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Database.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Database.class,false);
          if(toRet == null) {
            toRet = new DatabaseImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Database)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.DatabaseImpl expected");
        }
      }
      return (Database)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/id");
  }

  public LocalDateTime getReleaseDate() {
    return this.getDateTimeLit("http://gbol.life/0.1/releaseDate",true);
  }

  public void setReleaseDate(LocalDateTime val) {
    this.setDateTimeLit("http://gbol.life/0.1/releaseDate",val);
  }

  public String getVersion() {
    return this.getStringLit("http://gbol.life/0.1/version",true);
  }

  public void setVersion(String val) {
    this.setStringLit("http://gbol.life/0.1/version",val);
  }

  public String getId() {
    return this.getStringLit("http://gbol.life/0.1/id",false);
  }

  public void setId(String val) {
    this.setStringLit("http://gbol.life/0.1/id",val);
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }

  public String getLicense() {
    return this.getExternalRef("http://purl.org/dc/terms/license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("http://purl.org/dc/terms/license",val);
  }

  public String getHasVersion() {
    return this.getStringLit("http://purl.org/dc/terms/hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("http://purl.org/dc/terms/hasVersion",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriSpace",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("http://rdfs.org/ns/void#uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("http://rdfs.org/ns/void#uriRegexPattern",val);
  }

  public String getWaiver() {
    return this.getStringLit("http://vocab.org/waiver/terms/normswaiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("http://vocab.org/waiver/terms/normswaiver",val);
  }

  public String getNorms() {
    return this.getExternalRef("http://vocab.org/waiver/terms/normsnorms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("http://vocab.org/waiver/terms/normsnorms",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("http://purl.org/dc/terms/language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("http://purl.org/dc/terms/language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("http://purl.org/dc/terms/language",val);
  }

  public String getDescription() {
    return this.getStringLit("http://purl.org/dc/terms/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://purl.org/dc/terms/description",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",val);
  }

  public String getDataDump() {
    return this.getExternalRef("http://rdfs.org/ns/void#dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#dataDump",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getTitle() {
    return this.getStringLit("http://purl.org/dc/terms/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://purl.org/dc/terms/title",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }
}
