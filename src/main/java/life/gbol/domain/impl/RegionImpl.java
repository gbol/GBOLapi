package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Position;
import life.gbol.domain.Region;
import life.gbol.domain.Sequence;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class RegionImpl extends LocationImpl implements Region {
  public static final String TypeIRI = "http://gbol.life/0.1/Region";

  protected RegionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Region make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new RegionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Region.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Region.class,false);
          if(toRet == null) {
            toRet = new RegionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Region)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.RegionImpl expected");
        }
      }
      return (Region)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/end");
    this.checkCardMin1("http://gbol.life/0.1/begin");
  }

  public StrandPosition getStrand() {
    return this.getEnum("http://gbol.life/0.1/strand",true,StrandPosition.class);
  }

  public void setStrand(StrandPosition val) {
    this.setEnum("http://gbol.life/0.1/strand",val,StrandPosition.class);
  }

  public Position getEnd() {
    return this.getRef("http://gbol.life/0.1/end",false,Position.class);
  }

  public void setEnd(Position val) {
    this.setRef("http://gbol.life/0.1/end",val,Position.class);
  }

  public Position getBegin() {
    return this.getRef("http://gbol.life/0.1/begin",false,Position.class);
  }

  public void setBegin(Position val) {
    this.setRef("http://gbol.life/0.1/begin",val,Position.class);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
