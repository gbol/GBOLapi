package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.Citation;
import life.gbol.domain.Database;
import life.gbol.domain.EntryType;
import life.gbol.domain.GBOLDataSet;
import life.gbol.domain.Note;
import life.gbol.domain.Organism;
import life.gbol.domain.Sample;
import life.gbol.domain.Sequence;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class GBOLDataSetImpl extends EntityImpl implements GBOLDataSet {
  public static final String TypeIRI = "http://gbol.life/0.1/GBOLDataSet";

  protected GBOLDataSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static GBOLDataSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new GBOLDataSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,GBOLDataSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,GBOLDataSet.class,false);
          if(toRet == null) {
            toRet = new GBOLDataSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof GBOLDataSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.GBOLDataSetImpl expected");
        }
      }
      return (GBOLDataSet)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/entryType");
    this.checkCardMin1("http://gbol.life/0.1/dataSetVersion");
  }

  public void remReferences(Citation val) {
    this.remRef("http://gbol.life/0.1/references",val,true);
  }

  public List<? extends Citation> getAllReferences() {
    return this.getRefSet("http://gbol.life/0.1/references",true,Citation.class);
  }

  public void addReferences(Citation val) {
    this.addRef("http://gbol.life/0.1/references",val);
  }

  public EntryType getEntryType() {
    return this.getEnum("http://gbol.life/0.1/entryType",false,EntryType.class);
  }

  public void setEntryType(EntryType val) {
    this.setEnum("http://gbol.life/0.1/entryType",val,EntryType.class);
  }

  public void remSequences(Sequence val) {
    this.remRef("http://gbol.life/0.1/sequences",val,true);
  }

  public List<? extends Sequence> getAllSequences() {
    return this.getRefSet("http://gbol.life/0.1/sequences",true,Sequence.class);
  }

  public void addSequences(Sequence val) {
    this.addRef("http://gbol.life/0.1/sequences",val);
  }

  public void remLinkedDataBases(Database val) {
    this.remRef("http://gbol.life/0.1/linkedDataBases",val,true);
  }

  public List<? extends Database> getAllLinkedDataBases() {
    return this.getRefSet("http://gbol.life/0.1/linkedDataBases",true,Database.class);
  }

  public void addLinkedDataBases(Database val) {
    this.addRef("http://gbol.life/0.1/linkedDataBases",val);
  }

  public void remSamples(Sample val) {
    this.remRef("http://gbol.life/0.1/samples",val,true);
  }

  public List<? extends Sample> getAllSamples() {
    return this.getRefSet("http://gbol.life/0.1/samples",true,Sample.class);
  }

  public void addSamples(Sample val) {
    this.addRef("http://gbol.life/0.1/samples",val);
  }

  public Integer getDataSetVersion() {
    return this.getIntegerLit("http://gbol.life/0.1/dataSetVersion",false);
  }

  public void setDataSetVersion(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/dataSetVersion",val);
  }

  public void remAnnotationResults(AnnotationResult val) {
    this.remRef("http://gbol.life/0.1/annotationResults",val,true);
  }

  public List<? extends AnnotationResult> getAllAnnotationResults() {
    return this.getRefSet("http://gbol.life/0.1/annotationResults",true,AnnotationResult.class);
  }

  public void addAnnotationResults(AnnotationResult val) {
    this.addRef("http://gbol.life/0.1/annotationResults",val);
  }

  public void remOrganisms(Organism val) {
    this.remRef("http://gbol.life/0.1/organisms",val,true);
  }

  public List<? extends Organism> getAllOrganisms() {
    return this.getRefSet("http://gbol.life/0.1/organisms",true,Organism.class);
  }

  public void addOrganisms(Organism val) {
    this.addRef("http://gbol.life/0.1/organisms",val);
  }

  public void remKeywords(String val) {
    this.remStringLit("http://gbol.life/0.1/keywords",val,true);
  }

  public List<? extends String> getAllKeywords() {
    return this.getStringLitSet("http://gbol.life/0.1/keywords",true);
  }

  public void addKeywords(String val) {
    this.addStringLit("http://gbol.life/0.1/keywords",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }

  public String getLicense() {
    return this.getExternalRef("http://purl.org/dc/terms/license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("http://purl.org/dc/terms/license",val);
  }

  public String getHasVersion() {
    return this.getStringLit("http://purl.org/dc/terms/hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("http://purl.org/dc/terms/hasVersion",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriSpace",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("http://rdfs.org/ns/void#uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("http://rdfs.org/ns/void#uriRegexPattern",val);
  }

  public String getWaiver() {
    return this.getStringLit("http://vocab.org/waiver/terms/normswaiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("http://vocab.org/waiver/terms/normswaiver",val);
  }

  public String getNorms() {
    return this.getExternalRef("http://vocab.org/waiver/terms/normsnorms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("http://vocab.org/waiver/terms/normsnorms",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("http://purl.org/dc/terms/language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("http://purl.org/dc/terms/language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("http://purl.org/dc/terms/language",val);
  }

  public String getDescription() {
    return this.getStringLit("http://purl.org/dc/terms/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://purl.org/dc/terms/description",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",val);
  }

  public String getDataDump() {
    return this.getExternalRef("http://rdfs.org/ns/void#dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#dataDump",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getTitle() {
    return this.getStringLit("http://purl.org/dc/terms/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://purl.org/dc/terms/title",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }
}
