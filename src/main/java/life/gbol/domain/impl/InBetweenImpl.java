package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.InBetween;
import life.gbol.domain.Position;
import life.gbol.domain.Sequence;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class InBetweenImpl extends LocationImpl implements InBetween {
  public static final String TypeIRI = "http://gbol.life/0.1/InBetween";

  protected InBetweenImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static InBetween make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InBetweenImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,InBetween.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,InBetween.class,false);
          if(toRet == null) {
            toRet = new InBetweenImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof InBetween)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.InBetweenImpl expected");
        }
      }
      return (InBetween)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/after");
  }

  public Position getAfter() {
    return this.getRef("http://gbol.life/0.1/after",false,Position.class);
  }

  public void setAfter(Position val) {
    this.setRef("http://gbol.life/0.1/after",val,Position.class);
  }

  public StrandPosition getStrand() {
    return this.getEnum("http://gbol.life/0.1/strand",true,StrandPosition.class);
  }

  public void setStrand(StrandPosition val) {
    this.setEnum("http://gbol.life/0.1/strand",val,StrandPosition.class);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
