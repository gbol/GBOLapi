package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Organism;
import life.gbol.domain.Taxon;
import life.gbol.domain.TaxonomyRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class OrganismImpl extends OWLThingImpl implements Organism {
  public static final String TypeIRI = "http://gbol.life/0.1/Organism";

  protected OrganismImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Organism make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new OrganismImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Organism.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Organism.class,false);
          if(toRet == null) {
            toRet = new OrganismImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Organism)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.OrganismImpl expected");
        }
      }
      return (Organism)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public void remTaxonomyLineage(Taxon val) {
    this.remRef("http://gbol.life/0.1/taxonomyLineage",val,true);
  }

  public List<? extends Taxon> getAllTaxonomyLineage() {
    return this.getRefSet("http://gbol.life/0.1/taxonomyLineage",true,Taxon.class);
  }

  public void addTaxonomyLineage(Taxon val) {
    this.addRef("http://gbol.life/0.1/taxonomyLineage",val);
  }

  public TaxonomyRef getTaxonomy() {
    return this.getRef("http://gbol.life/0.1/taxonomy",true,TaxonomyRef.class);
  }

  public void setTaxonomy(TaxonomyRef val) {
    this.setRef("http://gbol.life/0.1/taxonomy",val,TaxonomyRef.class);
  }

  public String getScientificName() {
    return this.getStringLit("http://gbol.life/0.1/scientificName",true);
  }

  public void setScientificName(String val) {
    this.setStringLit("http://gbol.life/0.1/scientificName",val);
  }
}
