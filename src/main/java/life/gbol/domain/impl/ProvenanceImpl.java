package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.Provenance;
import life.gbol.domain.ProvenanceApplication;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceImpl extends OWLThingImpl implements Provenance {
  public static final String TypeIRI = "http://gbol.life/0.1/Provenance";

  protected ProvenanceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Provenance make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Provenance.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Provenance.class,false);
          if(toRet == null) {
            toRet = new ProvenanceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Provenance)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceImpl expected");
        }
      }
      return (Provenance)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/origin");
  }

  public AnnotationResult getOrigin() {
    return this.getRef("http://gbol.life/0.1/origin",false,AnnotationResult.class);
  }

  public void setOrigin(AnnotationResult val) {
    this.setRef("http://gbol.life/0.1/origin",val,AnnotationResult.class);
  }

  public ProvenanceApplication getAnnotation() {
    return this.getRef("http://gbol.life/0.1/annotation",true,ProvenanceApplication.class);
  }

  public void setAnnotation(ProvenanceApplication val) {
    this.setRef("http://gbol.life/0.1/annotation",val,ProvenanceApplication.class);
  }
}
