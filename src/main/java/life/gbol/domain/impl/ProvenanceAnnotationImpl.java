package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.ProvenanceAnnotation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.purl.ontology.bibo.domain.Document;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceAnnotationImpl extends ProvenanceApplicationImpl implements ProvenanceAnnotation {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceAnnotation";

  protected ProvenanceAnnotationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceAnnotation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceAnnotationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceAnnotation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceAnnotation.class,false);
          if(toRet == null) {
            toRet = new ProvenanceAnnotationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceAnnotation)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceAnnotationImpl expected");
        }
      }
      return (ProvenanceAnnotation)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/reference");
  }

  public Document getReference() {
    return this.getRef("http://gbol.life/0.1/reference",false,Document.class);
  }

  public void setReference(Document val) {
    this.setRef("http://gbol.life/0.1/reference",val,Document.class);
  }

  public void remDerivedFrom(Feature val) {
    this.remRef("http://gbol.life/0.1/derivedFrom",val,true);
  }

  public List<? extends Feature> getAllDerivedFrom() {
    return this.getRefSet("http://gbol.life/0.1/derivedFrom",true,Feature.class);
  }

  public void addDerivedFrom(Feature val) {
    this.addRef("http://gbol.life/0.1/derivedFrom",val);
  }

  public String getExperimentalEvidence() {
    return this.getStringLit("http://gbol.life/0.1/experimentalEvidence",true);
  }

  public void setExperimentalEvidence(String val) {
    this.setStringLit("http://gbol.life/0.1/experimentalEvidence",val);
  }

  public String getProvenanceNote() {
    return this.getStringLit("http://gbol.life/0.1/provenanceNote",true);
  }

  public void setProvenanceNote(String val) {
    this.setStringLit("http://gbol.life/0.1/provenanceNote",val);
  }
}
