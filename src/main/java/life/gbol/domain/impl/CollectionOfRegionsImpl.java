package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.CollectionOfRegions;
import life.gbol.domain.Region;
import life.gbol.domain.Sequence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class CollectionOfRegionsImpl extends LocationImpl implements CollectionOfRegions {
  public static final String TypeIRI = "http://gbol.life/0.1/CollectionOfRegions";

  protected CollectionOfRegionsImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static CollectionOfRegions make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CollectionOfRegionsImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,CollectionOfRegions.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,CollectionOfRegions.class,false);
          if(toRet == null) {
            toRet = new CollectionOfRegionsImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof CollectionOfRegions)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.CollectionOfRegionsImpl expected");
        }
      }
      return (CollectionOfRegions)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/members");
  }

  public void remMembers(Region val) {
    this.remRef("http://gbol.life/0.1/members",val,false);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefSet("http://gbol.life/0.1/members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRef("http://gbol.life/0.1/members",val);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
