package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.ProvenanceApplication;
import life.gbol.domain.XRefProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class XRefProvenanceImpl extends ProvenanceImpl implements XRefProvenance {
  public static final String TypeIRI = "http://gbol.life/0.1/XRefProvenance";

  protected XRefProvenanceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static XRefProvenance make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new XRefProvenanceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,XRefProvenance.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,XRefProvenance.class,false);
          if(toRet == null) {
            toRet = new XRefProvenanceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof XRefProvenance)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.XRefProvenanceImpl expected");
        }
      }
      return (XRefProvenance)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/origin");
  }

  public AnnotationLinkSet getOrigin() {
    return this.getRef("http://gbol.life/0.1/origin",false,AnnotationLinkSet.class);
  }

  public void setOrigin(AnnotationLinkSet val) {
    this.setRef("http://gbol.life/0.1/origin",val,AnnotationLinkSet.class);
  }

  public ProvenanceApplication getAnnotation() {
    return this.getRef("http://gbol.life/0.1/annotation",true,ProvenanceApplication.class);
  }

  public void setAnnotation(ProvenanceApplication val) {
    this.setRef("http://gbol.life/0.1/annotation",val,ProvenanceApplication.class);
  }
}
