package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.ASVAssignment;
import life.gbol.domain.Taxon;
import life.gbol.domain.TaxonAssignmentType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ASVAssignmentImpl extends ProvenanceClassificationImpl implements ASVAssignment {
  public static final String TypeIRI = "http://gbol.life/0.1/ASVAssignment";

  protected ASVAssignmentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ASVAssignment make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ASVAssignmentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ASVAssignment.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ASVAssignment.class,false);
          if(toRet == null) {
            toRet = new ASVAssignmentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ASVAssignment)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ASVAssignmentImpl expected");
        }
      }
      return (ASVAssignment)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/ratio");
    this.checkCardMin1("http://gbol.life/0.1/taxon");
    this.checkCardMin1("http://gbol.life/0.1/type");
    this.checkCardMin1("http://gbol.life/0.1/numberHits");
    this.checkCardMin1("http://gbol.life/0.1/levelCount");
  }

  public Float getRatio() {
    return this.getFloatLit("http://gbol.life/0.1/ratio",false);
  }

  public void setRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/ratio",val);
  }

  public Taxon getTaxon() {
    return this.getRef("http://gbol.life/0.1/taxon",false,Taxon.class);
  }

  public void setTaxon(Taxon val) {
    this.setRef("http://gbol.life/0.1/taxon",val,Taxon.class);
  }

  public TaxonAssignmentType getType() {
    return this.getEnum("http://gbol.life/0.1/type",false,TaxonAssignmentType.class);
  }

  public void setType(TaxonAssignmentType val) {
    this.setEnum("http://gbol.life/0.1/type",val,TaxonAssignmentType.class);
  }

  public Integer getNumberHits() {
    return this.getIntegerLit("http://gbol.life/0.1/numberHits",false);
  }

  public void setNumberHits(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/numberHits",val);
  }

  public Integer getLevelCount() {
    return this.getIntegerLit("http://gbol.life/0.1/levelCount",false);
  }

  public void setLevelCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/levelCount",val);
  }
}
