package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Base;
import life.gbol.domain.Position;
import life.gbol.domain.Sequence;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class BaseImpl extends LocationImpl implements Base {
  public static final String TypeIRI = "http://gbol.life/0.1/Base";

  protected BaseImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Base make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new BaseImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Base.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Base.class,false);
          if(toRet == null) {
            toRet = new BaseImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Base)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.BaseImpl expected");
        }
      }
      return (Base)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/at");
  }

  public Position getAt() {
    return this.getRef("http://gbol.life/0.1/at",false,Position.class);
  }

  public void setAt(Position val) {
    this.setRef("http://gbol.life/0.1/at",val,Position.class);
  }

  public StrandPosition getStrand() {
    return this.getEnum("http://gbol.life/0.1/strand",true,StrandPosition.class);
  }

  public void setStrand(StrandPosition val) {
    this.setEnum("http://gbol.life/0.1/strand",val,StrandPosition.class);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
