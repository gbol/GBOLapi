package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.Long;
import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.FileType;
import life.gbol.domain.ReadOrientation;
import life.gbol.domain.RemoteSequenceFile;
import life.gbol.domain.SequencingPlatform;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class RemoteSequenceFileImpl extends RemoteDataFileImpl implements RemoteSequenceFile {
  public static final String TypeIRI = "http://gbol.life/0.1/RemoteSequenceFile";

  protected RemoteSequenceFileImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static RemoteSequenceFile make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new RemoteSequenceFileImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,RemoteSequenceFile.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,RemoteSequenceFile.class,false);
          if(toRet == null) {
            toRet = new RemoteSequenceFileImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof RemoteSequenceFile)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.RemoteSequenceFileImpl expected");
        }
      }
      return (RemoteSequenceFile)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/reads");
    this.checkCardMin1("http://gbol.life/0.1/bases");
    this.checkCardMin1("http://gbol.life/0.1/readLength");
  }

  public Long getReads() {
    return this.getLongLit("http://gbol.life/0.1/reads",false);
  }

  public void setReads(Long val) {
    this.setLongLit("http://gbol.life/0.1/reads",val);
  }

  public SequencingPlatform getSequencer() {
    return this.getEnum("http://gbol.life/0.1/sequencer",true,SequencingPlatform.class);
  }

  public void setSequencer(SequencingPlatform val) {
    this.setEnum("http://gbol.life/0.1/sequencer",val,SequencingPlatform.class);
  }

  public Long getBases() {
    return this.getLongLit("http://gbol.life/0.1/bases",false);
  }

  public void setBases(Long val) {
    this.setLongLit("http://gbol.life/0.1/bases",val);
  }

  public Long getReadLength() {
    return this.getLongLit("http://gbol.life/0.1/readLength",false);
  }

  public void setReadLength(Long val) {
    this.setLongLit("http://gbol.life/0.1/readLength",val);
  }

  public Integer getSequencingDepth() {
    return this.getIntegerLit("http://gbol.life/0.1/sequencingDepth",true);
  }

  public void setSequencingDepth(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/sequencingDepth",val);
  }

  public ReadOrientation getStrandOrientation() {
    return this.getEnum("http://gbol.life/0.1/StrandOrientation",true,ReadOrientation.class);
  }

  public void setStrandOrientation(ReadOrientation val) {
    this.setEnum("http://gbol.life/0.1/StrandOrientation",val,ReadOrientation.class);
  }

  public String getAdapter() {
    return this.getStringLit("http://gbol.life/0.1/adapter",true);
  }

  public void setAdapter(String val) {
    this.setStringLit("http://gbol.life/0.1/adapter",val);
  }

  public String getRemoteFileName() {
    return this.getStringLit("http://gbol.life/0.1/remoteFileName",false);
  }

  public void setRemoteFileName(String val) {
    this.setStringLit("http://gbol.life/0.1/remoteFileName",val);
  }

  public LocalDateTime getRetrievedOn() {
    return this.getDateTimeLit("http://gbol.life/0.1/retrievedOn",true);
  }

  public void setRetrievedOn(LocalDateTime val) {
    this.setDateTimeLit("http://gbol.life/0.1/retrievedOn",val);
  }

  public String getMd5() {
    return this.getStringLit("http://gbol.life/0.1/md5",true);
  }

  public void setMd5(String val) {
    this.setStringLit("http://gbol.life/0.1/md5",val);
  }

  public FileType getFileType() {
    return this.getEnum("http://gbol.life/0.1/fileType",true,FileType.class);
  }

  public void setFileType(FileType val) {
    this.setEnum("http://gbol.life/0.1/fileType",val,FileType.class);
  }

  public Long getSize() {
    return this.getLongLit("http://gbol.life/0.1/size",true);
  }

  public void setSize(Long val) {
    this.setLongLit("http://gbol.life/0.1/size",val);
  }

  public String getRetrievedFrom() {
    return this.getExternalRef("http://gbol.life/0.1/retrievedFrom",true);
  }

  public void setRetrievedFrom(String val) {
    this.setExternalRef("http://gbol.life/0.1/retrievedFrom",val);
  }

  public String getRemoteFilePath() {
    return this.getStringLit("http://gbol.life/0.1/remoteFilePath",false);
  }

  public void setRemoteFilePath(String val) {
    this.setStringLit("http://gbol.life/0.1/remoteFilePath",val);
  }

  public void remContainedEntity(Entity val) {
    this.remRef("http://gbol.life/0.1/containedEntity",val,true);
  }

  public List<? extends Entity> getAllContainedEntity() {
    return this.getRefSet("http://gbol.life/0.1/containedEntity",true,Entity.class);
  }

  public void addContainedEntity(Entity val) {
    this.addRef("http://gbol.life/0.1/containedEntity",val);
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }

  public String getLicense() {
    return this.getExternalRef("http://purl.org/dc/terms/license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("http://purl.org/dc/terms/license",val);
  }

  public String getHasVersion() {
    return this.getStringLit("http://purl.org/dc/terms/hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("http://purl.org/dc/terms/hasVersion",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriSpace",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("http://rdfs.org/ns/void#uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("http://rdfs.org/ns/void#uriRegexPattern",val);
  }

  public String getWaiver() {
    return this.getStringLit("http://vocab.org/waiver/terms/normswaiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("http://vocab.org/waiver/terms/normswaiver",val);
  }

  public String getNorms() {
    return this.getExternalRef("http://vocab.org/waiver/terms/normsnorms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("http://vocab.org/waiver/terms/normsnorms",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("http://purl.org/dc/terms/language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("http://purl.org/dc/terms/language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("http://purl.org/dc/terms/language",val);
  }

  public String getDescription() {
    return this.getStringLit("http://purl.org/dc/terms/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://purl.org/dc/terms/description",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#uriLookupEndpoint",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#sparqlEndpoint",val);
  }

  public String getDataDump() {
    return this.getExternalRef("http://rdfs.org/ns/void#dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("http://rdfs.org/ns/void#dataDump",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getTitle() {
    return this.getStringLit("http://purl.org/dc/terms/title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://purl.org/dc/terms/title",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }
}
