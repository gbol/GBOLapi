package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Barcode;
import life.gbol.domain.BarcodeSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class BarcodeSetImpl extends SequenceSetImpl implements BarcodeSet {
  public static final String TypeIRI = "http://gbol.life/0.1/BarcodeSet";

  protected BarcodeSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static BarcodeSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new BarcodeSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,BarcodeSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,BarcodeSet.class,false);
          if(toRet == null) {
            toRet = new BarcodeSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof BarcodeSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.BarcodeSetImpl expected");
        }
      }
      return (BarcodeSet)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Barcode getForwardBarcode() {
    return this.getRef("http://gbol.life/0.1/forwardBarcode",true,Barcode.class);
  }

  public void setForwardBarcode(Barcode val) {
    this.setRef("http://gbol.life/0.1/forwardBarcode",val,Barcode.class);
  }

  public Barcode getReverseBarcode() {
    return this.getRef("http://gbol.life/0.1/reverseBarcode",true,Barcode.class);
  }

  public void setReverseBarcode(Barcode val) {
    this.setRef("http://gbol.life/0.1/reverseBarcode",val,Barcode.class);
  }
}
