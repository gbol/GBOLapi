package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.CDS;
import life.gbol.domain.Citation;
import life.gbol.domain.Expression;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.Protein;
import life.gbol.domain.ReasonArtificialLocation;
import life.gbol.domain.TranslExcept;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class CDSImpl extends TranscriptFeatureImpl implements CDS {
  public static final String TypeIRI = "http://gbol.life/0.1/CDS";

  protected CDSImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static CDS make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CDSImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,CDS.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,CDS.class,false);
          if(toRet == null) {
            toRet = new CDSImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof CDS)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.CDSImpl expected");
        }
      }
      return (CDS)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/protein");
  }

  public String getProteinId() {
    return this.getStringLit("http://gbol.life/0.1/proteinId",true);
  }

  public void setProteinId(String val) {
    this.setStringLit("http://gbol.life/0.1/proteinId",val);
  }

  public Boolean getRibosomalSlippage() {
    return this.getBooleanLit("http://gbol.life/0.1/ribosomalSlippage",true);
  }

  public void setRibosomalSlippage(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/ribosomalSlippage",val);
  }

  public String getException() {
    return this.getStringLit("http://gbol.life/0.1/exception",true);
  }

  public void setException(String val) {
    this.setStringLit("http://gbol.life/0.1/exception",val);
  }

  public String getProduct() {
    return this.getStringLit("http://gbol.life/0.1/product",true);
  }

  public void setProduct(String val) {
    this.setStringLit("http://gbol.life/0.1/product",val);
  }

  public Integer getCodonStart() {
    return this.getIntegerLit("http://gbol.life/0.1/codonStart",true);
  }

  public void setCodonStart(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/codonStart",val);
  }

  public Protein getProtein() {
    return this.getRef("http://gbol.life/0.1/protein",false,Protein.class);
  }

  public void setProtein(Protein val) {
    this.setRef("http://gbol.life/0.1/protein",val,Protein.class);
  }

  public void remTranslExcept(TranslExcept val) {
    this.remRef("http://gbol.life/0.1/translExcept",val,true);
  }

  public List<? extends TranslExcept> getAllTranslExcept() {
    return this.getRefSet("http://gbol.life/0.1/translExcept",true,TranslExcept.class);
  }

  public void addTranslExcept(TranslExcept val) {
    this.addRef("http://gbol.life/0.1/translExcept",val);
  }

  public void remExpression(Expression val) {
    this.remRef("http://gbol.life/0.1/expression",val,true);
  }

  public List<? extends Expression> getAllExpression() {
    return this.getRefSet("http://gbol.life/0.1/expression",true,Expression.class);
  }

  public void addExpression(Expression val) {
    this.addRef("http://gbol.life/0.1/expression",val);
  }

  public String getMapLocation() {
    return this.getStringLit("http://gbol.life/0.1/mapLocation",true);
  }

  public void setMapLocation(String val) {
    this.setStringLit("http://gbol.life/0.1/mapLocation",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public void remProvenance(FeatureProvenance val) {
    this.remRef("http://gbol.life/0.1/provenance",val,false);
  }

  public List<? extends FeatureProvenance> getAllProvenance() {
    return this.getRefSet("http://gbol.life/0.1/provenance",false,FeatureProvenance.class);
  }

  public void addProvenance(FeatureProvenance val) {
    this.addRef("http://gbol.life/0.1/provenance",val);
  }

  public ReasonArtificialLocation getArtificialLocation() {
    return this.getEnum("http://gbol.life/0.1/artificialLocation",true,ReasonArtificialLocation.class);
  }

  public void setArtificialLocation(ReasonArtificialLocation val) {
    this.setEnum("http://gbol.life/0.1/artificialLocation",val,ReasonArtificialLocation.class);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
