package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Curator;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.impl.PersonImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class CuratorImpl extends PersonImpl implements Curator {
  public static final String TypeIRI = "http://gbol.life/0.1/Curator";

  protected CuratorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Curator make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CuratorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Curator.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Curator.class,false);
          if(toRet == null) {
            toRet = new CuratorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Curator)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.CuratorImpl expected");
        }
      }
      return (Curator)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/orcid");
  }

  public String getOrcid() {
    return this.getExternalRef("http://gbol.life/0.1/orcid",false);
  }

  public void setOrcid(String val) {
    this.setExternalRef("http://gbol.life/0.1/orcid",val);
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
