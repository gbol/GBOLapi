package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.AnnotationActivity;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.ActivityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AnnotationActivityImpl extends ActivityImpl implements AnnotationActivity {
  public static final String TypeIRI = "http://gbol.life/0.1/AnnotationActivity";

  protected AnnotationActivityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AnnotationActivity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationActivityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AnnotationActivity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AnnotationActivity.class,false);
          if(toRet == null) {
            toRet = new AnnotationActivityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AnnotationActivity)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationActivityImpl expected");
        }
      }
      return (AnnotationActivity)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://www.w3.org/ns/prov#startedAtTime");
    this.checkCardMin1("http://www.w3.org/ns/prov#endedAtTime");
  }

  public LocalDateTime getStartedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",false);
  }

  public void setStartedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public LocalDateTime getEndedAtTime() {
    return this.getDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",false);
  }

  public void setEndedAtTime(LocalDateTime val) {
    this.setDateTimeLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }

  public Agent getWasAssociatedWith() {
    return this.getRef("http://gbol.life/0.1/wasAssociatedWith",true,Agent.class);
  }

  public void setWasAssociatedWith(Agent val) {
    this.setRef("http://gbol.life/0.1/wasAssociatedWith",val,Agent.class);
  }

  public void remUsed(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#used",val,true);
  }

  public List<? extends Entity> getAllUsed() {
    return this.getRefSet("http://www.w3.org/ns/prov#used",true,Entity.class);
  }

  public void addUsed(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#used",val);
  }

  public void remGenerated(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#generated",val,true);
  }

  public List<? extends Entity> getAllGenerated() {
    return this.getRefSet("http://www.w3.org/ns/prov#generated",true,Entity.class);
  }

  public void addGenerated(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#generated",val);
  }

  public Activity getWasInformedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasInformedBy",true,Activity.class);
  }

  public void setWasInformedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasInformedBy",val,Activity.class);
  }
}
