package life.gbol.domain.impl;

import java.lang.Long;
import java.lang.String;
import java.util.List;
import life.gbol.domain.LocalSequenceFile;
import life.gbol.domain.SingleEnd;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SingleEndImpl extends ReadInformationImpl implements SingleEnd {
  public static final String TypeIRI = "http://gbol.life/0.1/SingleEnd";

  protected SingleEndImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SingleEnd make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SingleEndImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SingleEnd.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SingleEnd.class,false);
          if(toRet == null) {
            toRet = new SingleEndImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SingleEnd)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SingleEndImpl expected");
        }
      }
      return (SingleEnd)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getCenter() {
    return this.getStringLit("http://gbol.life/0.1/center",true);
  }

  public void setCenter(String val) {
    this.setStringLit("http://gbol.life/0.1/center",val);
  }

  public Long getBases() {
    return this.getLongLit("http://gbol.life/0.1/bases",true);
  }

  public void setBases(Long val) {
    this.setLongLit("http://gbol.life/0.1/bases",val);
  }

  public void remFile(LocalSequenceFile val) {
    this.remRef("http://gbol.life/0.1/file",val,false);
  }

  public List<? extends LocalSequenceFile> getAllFile() {
    return this.getRefSet("http://gbol.life/0.1/file",false,LocalSequenceFile.class);
  }

  public void addFile(LocalSequenceFile val) {
    this.addRef("http://gbol.life/0.1/file",val);
  }

  public Long getReadLength() {
    return this.getLongLit("http://gbol.life/0.1/readLength",true);
  }

  public void setReadLength(Long val) {
    this.setLongLit("http://gbol.life/0.1/readLength",val);
  }

  public String getMachine() {
    return this.getStringLit("http://gbol.life/0.1/machine",true);
  }

  public void setMachine(String val) {
    this.setStringLit("http://gbol.life/0.1/machine",val);
  }

  public String getAdapter() {
    return this.getStringLit("http://gbol.life/0.1/adapter",true);
  }

  public void setAdapter(String val) {
    this.setStringLit("http://gbol.life/0.1/adapter",val);
  }
}
