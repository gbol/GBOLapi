package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.Experiment;
import life.gbol.domain.Expression;
import life.gbol.domain.ExpressionType;
import life.gbol.domain.ProvenanceApplication;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ExpressionImpl extends OWLThingImpl implements Expression {
  public static final String TypeIRI = "http://gbol.life/0.1/Expression";

  protected ExpressionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Expression make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExpressionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Expression.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Expression.class,false);
          if(toRet == null) {
            toRet = new ExpressionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Expression)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ExpressionImpl expected");
        }
      }
      return (Expression)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/experiment");
    this.checkCardMin1("http://gbol.life/0.1/expressionType");
    this.checkCardMin1("http://gbol.life/0.1/annotation");
    this.checkCardMin1("http://gbol.life/0.1/value");
  }

  public Experiment getExperiment() {
    return this.getRef("http://gbol.life/0.1/experiment",false,Experiment.class);
  }

  public void setExperiment(Experiment val) {
    this.setRef("http://gbol.life/0.1/experiment",val,Experiment.class);
  }

  public ExpressionType getExpressionType() {
    return this.getEnum("http://gbol.life/0.1/expressionType",false,ExpressionType.class);
  }

  public void setExpressionType(ExpressionType val) {
    this.setEnum("http://gbol.life/0.1/expressionType",val,ExpressionType.class);
  }

  public ProvenanceApplication getAnnotation() {
    return this.getRef("http://gbol.life/0.1/annotation",false,ProvenanceApplication.class);
  }

  public void setAnnotation(ProvenanceApplication val) {
    this.setRef("http://gbol.life/0.1/annotation",val,ProvenanceApplication.class);
  }

  public Double getValue() {
    return this.getDoubleLit("http://gbol.life/0.1/value",false);
  }

  public void setValue(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/value",val);
  }
}
