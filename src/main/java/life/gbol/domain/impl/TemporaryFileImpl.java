package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TemporaryFile;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class TemporaryFileImpl extends EntityImpl implements TemporaryFile {
  public static final String TypeIRI = "http://gbol.life/0.1/TemporaryFile";

  protected TemporaryFileImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TemporaryFile make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TemporaryFileImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TemporaryFile.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TemporaryFile.class,false);
          if(toRet == null) {
            toRet = new TemporaryFileImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TemporaryFile)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.TemporaryFileImpl expected");
        }
      }
      return (TemporaryFile)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://www.w3.org/ns/prov#wasDerivedFrom");
  }

  public Entity getWasDerivedFrom() {
    return this.getRef("http://www.w3.org/ns/prov#wasDerivedFrom",false,Entity.class);
  }

  public void setWasDerivedFrom(Entity val) {
    this.setRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,Entity.class);
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }
}
