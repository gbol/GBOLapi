package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Organism;
import life.gbol.domain.VariantGenotype;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class VariantGenotypeImpl extends OWLThingImpl implements VariantGenotype {
  public static final String TypeIRI = "http://gbol.life/0.1/VariantGenotype";

  protected VariantGenotypeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static VariantGenotype make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new VariantGenotypeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,VariantGenotype.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,VariantGenotype.class,false);
          if(toRet == null) {
            toRet = new VariantGenotypeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof VariantGenotype)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.VariantGenotypeImpl expected");
        }
      }
      return (VariantGenotype)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getExpectedAlleleCounts() {
    return this.getStringLit("http://gbol.life/0.1/expectedAlleleCounts",true);
  }

  public void setExpectedAlleleCounts(String val) {
    this.setStringLit("http://gbol.life/0.1/expectedAlleleCounts",val);
  }

  public void remGenotype(VariantGenotype val) {
    this.remRef("http://gbol.life/0.1/genotype",val,true);
  }

  public List<? extends VariantGenotype> getAllGenotype() {
    return this.getRefSet("http://gbol.life/0.1/genotype",true,VariantGenotype.class);
  }

  public void addGenotype(VariantGenotype val) {
    this.addRef("http://gbol.life/0.1/genotype",val);
  }

  public String getStrain() {
    return this.getStringLit("http://gbol.life/0.1/strain",true);
  }

  public void setStrain(String val) {
    this.setStringLit("http://gbol.life/0.1/strain",val);
  }

  public Organism getAltOrganism() {
    return this.getRef("http://gbol.life/0.1/altOrganism",true,Organism.class);
  }

  public void setAltOrganism(Organism val) {
    this.setRef("http://gbol.life/0.1/altOrganism",val,Organism.class);
  }

  public String getAltDepth() {
    return this.getStringLit("http://gbol.life/0.1/altDepth",true);
  }

  public void setAltDepth(String val) {
    this.setStringLit("http://gbol.life/0.1/altDepth",val);
  }

  public Integer getPhredLikelihood() {
    return this.getIntegerLit("http://gbol.life/0.1/phredLikelihood",true);
  }

  public void setPhredLikelihood(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/phredLikelihood",val);
  }

  public String getReadDepth() {
    return this.getStringLit("http://gbol.life/0.1/readDepth",true);
  }

  public void setReadDepth(String val) {
    this.setStringLit("http://gbol.life/0.1/readDepth",val);
  }

  public String getMapQuality() {
    return this.getStringLit("http://gbol.life/0.1/mapQuality",true);
  }

  public void setMapQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/mapQuality",val);
  }

  public String getDepthQuality() {
    return this.getStringLit("http://gbol.life/0.1/depthQuality",true);
  }

  public void setDepthQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/depthQuality",val);
  }

  public String getGenotypeName() {
    return this.getStringLit("http://gbol.life/0.1/genotypeName",true);
  }

  public void setGenotypeName(String val) {
    this.setStringLit("http://gbol.life/0.1/genotypeName",val);
  }

  public String getLikelihood() {
    return this.getStringLit("http://gbol.life/0.1/likelihood",true);
  }

  public void setLikelihood(String val) {
    this.setStringLit("http://gbol.life/0.1/likelihood",val);
  }

  public String getGenotypeQuality() {
    return this.getStringLit("http://gbol.life/0.1/genotypeQuality",true);
  }

  public void setGenotypeQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/genotypeQuality",val);
  }

  public String getRefDepth() {
    return this.getStringLit("http://gbol.life/0.1/refDepth",true);
  }

  public void setRefDepth(String val) {
    this.setStringLit("http://gbol.life/0.1/refDepth",val);
  }

  public String getHaploQuality() {
    return this.getStringLit("http://gbol.life/0.1/haploQuality",true);
  }

  public void setHaploQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/haploQuality",val);
  }

  public String getPhaseSet() {
    return this.getStringLit("http://gbol.life/0.1/phaseSet",true);
  }

  public void setPhaseSet(String val) {
    this.setStringLit("http://gbol.life/0.1/phaseSet",val);
  }

  public String getPVal() {
    return this.getStringLit("http://gbol.life/0.1/pVal",true);
  }

  public void setPVal(String val) {
    this.setStringLit("http://gbol.life/0.1/pVal",val);
  }

  public Integer getPhaseQuality() {
    return this.getIntegerLit("http://gbol.life/0.1/phaseQuality",true);
  }

  public void setPhaseQuality(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/phaseQuality",val);
  }

  public String getFilter() {
    return this.getStringLit("http://gbol.life/0.1/filter",true);
  }

  public void setFilter(String val) {
    this.setStringLit("http://gbol.life/0.1/filter",val);
  }

  public Float getPhredProb() {
    return this.getFloatLit("http://gbol.life/0.1/phredProb",true);
  }

  public void setPhredProb(Float val) {
    this.setFloatLit("http://gbol.life/0.1/phredProb",val);
  }

  public String getFreq() {
    return this.getStringLit("http://gbol.life/0.1/freq",true);
  }

  public void setFreq(String val) {
    this.setStringLit("http://gbol.life/0.1/freq",val);
  }

  public String getRealGenotype() {
    return this.getStringLit("http://gbol.life/0.1/realGenotype",true);
  }

  public void setRealGenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/realGenotype",val);
  }

  public Organism getRefOrganism() {
    return this.getRef("http://gbol.life/0.1/refOrganism",true,Organism.class);
  }

  public void setRefOrganism(Organism val) {
    this.setRef("http://gbol.life/0.1/refOrganism",val,Organism.class);
  }
}
