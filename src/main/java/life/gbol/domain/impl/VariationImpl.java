package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.Expression;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.Position;
import life.gbol.domain.ReasonArtificialLocation;
import life.gbol.domain.VariantGenotype;
import life.gbol.domain.Variation;
import life.gbol.domain.VariationTypes;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class VariationImpl extends VariationFeatureImpl implements Variation {
  public static final String TypeIRI = "http://gbol.life/0.1/Variation";

  protected VariationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Variation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new VariationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Variation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Variation.class,false);
          if(toRet == null) {
            toRet = new VariationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Variation)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.VariationImpl expected");
        }
      }
      return (Variation)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/varType");
  }

  public void remGenotype(VariantGenotype val) {
    this.remRef("http://gbol.life/0.1/genotype",val,true);
  }

  public List<? extends VariantGenotype> getAllGenotype() {
    return this.getRefSet("http://gbol.life/0.1/genotype",true,VariantGenotype.class);
  }

  public void addGenotype(VariantGenotype val) {
    this.addRef("http://gbol.life/0.1/genotype",val);
  }

  public Integer getHomozygousSamples() {
    return this.getIntegerLit("http://gbol.life/0.1/homozygousSamples",true);
  }

  public void setHomozygousSamples(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/homozygousSamples",val);
  }

  public String getValidated() {
    return this.getStringLit("http://gbol.life/0.1/validated",true);
  }

  public void setValidated(String val) {
    this.setStringLit("http://gbol.life/0.1/validated",val);
  }

  public Position getEnd() {
    return this.getRef("http://gbol.life/0.1/end",true,Position.class);
  }

  public void setEnd(Position val) {
    this.setRef("http://gbol.life/0.1/end",val,Position.class);
  }

  public String getReadDepth() {
    return this.getStringLit("http://gbol.life/0.1/readDepth",true);
  }

  public void setReadDepth(String val) {
    this.setStringLit("http://gbol.life/0.1/readDepth",val);
  }

  public Integer getNumberOfSamples() {
    return this.getIntegerLit("http://gbol.life/0.1/numberOfSamples",true);
  }

  public void setNumberOfSamples(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/numberOfSamples",val);
  }

  public String getMapQuality() {
    return this.getStringLit("http://gbol.life/0.1/mapQuality",true);
  }

  public void setMapQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/mapQuality",val);
  }

  public String getAlleleFreq() {
    return this.getStringLit("http://gbol.life/0.1/alleleFreq",true);
  }

  public void setAlleleFreq(String val) {
    this.setStringLit("http://gbol.life/0.1/alleleFreq",val);
  }

  public String getReferenceAllele() {
    return this.getStringLit("http://gbol.life/0.1/referenceAllele",true);
  }

  public void setReferenceAllele(String val) {
    this.setStringLit("http://gbol.life/0.1/referenceAllele",val);
  }

  public String getAncestralAllele() {
    return this.getStringLit("http://gbol.life/0.1/ancestralAllele",true);
  }

  public void setAncestralAllele(String val) {
    this.setStringLit("http://gbol.life/0.1/ancestralAllele",val);
  }

  public String getQuality() {
    return this.getStringLit("http://gbol.life/0.1/quality",true);
  }

  public void setQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/quality",val);
  }

  public Integer getNumberNotCalled() {
    return this.getIntegerLit("http://gbol.life/0.1/numberNotCalled",true);
  }

  public void setNumberNotCalled(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/numberNotCalled",val);
  }

  public Integer getAlleleNumber() {
    return this.getIntegerLit("http://gbol.life/0.1/alleleNumber",true);
  }

  public void setAlleleNumber(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/alleleNumber",val);
  }

  public Integer getAlleleCount() {
    return this.getIntegerLit("http://gbol.life/0.1/alleleCount",true);
  }

  public void setAlleleCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/alleleCount",val);
  }

  public VariationTypes getVarType() {
    return this.getEnum("http://gbol.life/0.1/varType",false,VariationTypes.class);
  }

  public void setVarType(VariationTypes val) {
    this.setEnum("http://gbol.life/0.1/varType",val,VariationTypes.class);
  }

  public String getStrandBias() {
    return this.getStringLit("http://gbol.life/0.1/strandBias",true);
  }

  public void setStrandBias(String val) {
    this.setStringLit("http://gbol.life/0.1/strandBias",val);
  }

  public Integer getHeterozygousSamples() {
    return this.getIntegerLit("http://gbol.life/0.1/heterozygousSamples",true);
  }

  public void setHeterozygousSamples(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/heterozygousSamples",val);
  }

  public void remAlternate(String val) {
    this.remStringLit("http://gbol.life/0.1/alternate",val,true);
  }

  public List<? extends String> getAllAlternate() {
    return this.getStringLitSet("http://gbol.life/0.1/alternate",true);
  }

  public void addAlternate(String val) {
    this.addStringLit("http://gbol.life/0.1/alternate",val);
  }

  public String getDbSNP() {
    return this.getStringLit("http://gbol.life/0.1/dbSNP",true);
  }

  public void setDbSNP(String val) {
    this.setStringLit("http://gbol.life/0.1/dbSNP",val);
  }

  public String getCIGAR() {
    return this.getStringLit("http://gbol.life/0.1/CIGAR",true);
  }

  public void setCIGAR(String val) {
    this.setStringLit("http://gbol.life/0.1/CIGAR",val);
  }

  public String getID() {
    return this.getStringLit("http://gbol.life/0.1/ID",true);
  }

  public void setID(String val) {
    this.setStringLit("http://gbol.life/0.1/ID",val);
  }

  public String getFilter() {
    return this.getStringLit("http://gbol.life/0.1/filter",true);
  }

  public void setFilter(String val) {
    this.setStringLit("http://gbol.life/0.1/filter",val);
  }

  public Integer getWildtypeSamples() {
    return this.getIntegerLit("http://gbol.life/0.1/wildtypeSamples",true);
  }

  public void setWildtypeSamples(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/wildtypeSamples",val);
  }

  public String getBaseQuality() {
    return this.getStringLit("http://gbol.life/0.1/baseQuality",true);
  }

  public void setBaseQuality(String val) {
    this.setStringLit("http://gbol.life/0.1/baseQuality",val);
  }

  public String getReplace() {
    return this.getStringLit("http://gbol.life/0.1/replace",true);
  }

  public void setReplace(String val) {
    this.setStringLit("http://gbol.life/0.1/replace",val);
  }

  public void remExpression(Expression val) {
    this.remRef("http://gbol.life/0.1/expression",val,true);
  }

  public List<? extends Expression> getAllExpression() {
    return this.getRefSet("http://gbol.life/0.1/expression",true,Expression.class);
  }

  public void addExpression(Expression val) {
    this.addRef("http://gbol.life/0.1/expression",val);
  }

  public String getMapLocation() {
    return this.getStringLit("http://gbol.life/0.1/mapLocation",true);
  }

  public void setMapLocation(String val) {
    this.setStringLit("http://gbol.life/0.1/mapLocation",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public void remProvenance(FeatureProvenance val) {
    this.remRef("http://gbol.life/0.1/provenance",val,false);
  }

  public List<? extends FeatureProvenance> getAllProvenance() {
    return this.getRefSet("http://gbol.life/0.1/provenance",false,FeatureProvenance.class);
  }

  public void addProvenance(FeatureProvenance val) {
    this.addRef("http://gbol.life/0.1/provenance",val);
  }

  public ReasonArtificialLocation getArtificialLocation() {
    return this.getEnum("http://gbol.life/0.1/artificialLocation",true,ReasonArtificialLocation.class);
  }

  public void setArtificialLocation(ReasonArtificialLocation val) {
    this.setEnum("http://gbol.life/0.1/artificialLocation",val,ReasonArtificialLocation.class);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
