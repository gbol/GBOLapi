package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Experiment;
import life.gbol.domain.Provenance;
import life.gbol.domain.ReadInformation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ExperimentImpl extends OWLThingImpl implements Experiment {
  public static final String TypeIRI = "http://gbol.life/0.1/Experiment";

  protected ExperimentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Experiment make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExperimentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Experiment.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Experiment.class,false);
          if(toRet == null) {
            toRet = new ExperimentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Experiment)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ExperimentImpl expected");
        }
      }
      return (Experiment)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/inputReads");
    this.checkCardMin1("http://gbol.life/0.1/provenance");
  }

  public ReadInformation getInputReads() {
    return this.getRef("http://gbol.life/0.1/inputReads",false,ReadInformation.class);
  }

  public void setInputReads(ReadInformation val) {
    this.setRef("http://gbol.life/0.1/inputReads",val,ReadInformation.class);
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",false,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }
}
