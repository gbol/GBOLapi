package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Location;
import life.gbol.domain.Sequence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class LocationImpl extends OWLThingImpl implements Location {
  public static final String TypeIRI = "http://gbol.life/0.1/Location";

  protected LocationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Location make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new LocationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Location.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Location.class,false);
          if(toRet == null) {
            toRet = new LocationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Location)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.LocationImpl expected");
        }
      }
      return (Location)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
