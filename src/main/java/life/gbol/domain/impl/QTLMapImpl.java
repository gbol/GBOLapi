package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.MapUnits;
import life.gbol.domain.Organism;
import life.gbol.domain.QTLMap;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class QTLMapImpl extends OWLThingImpl implements QTLMap {
  public static final String TypeIRI = "http://gbol.life/0.1/QTLMap";

  protected QTLMapImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static QTLMap make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new QTLMapImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,QTLMap.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,QTLMap.class,false);
          if(toRet == null) {
            toRet = new QTLMapImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof QTLMap)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.QTLMapImpl expected");
        }
      }
      return (QTLMap)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/units");
  }

  public String getMapAccession() {
    return this.getStringLit("http://gbol.life/0.1/mapAccession",true);
  }

  public void setMapAccession(String val) {
    this.setStringLit("http://gbol.life/0.1/mapAccession",val);
  }

  public MapUnits getUnits() {
    return this.getEnum("http://gbol.life/0.1/units",false,MapUnits.class);
  }

  public void setUnits(MapUnits val) {
    this.setEnum("http://gbol.life/0.1/units",val,MapUnits.class);
  }

  public String getMapName() {
    return this.getStringLit("http://gbol.life/0.1/mapName",true);
  }

  public void setMapName(String val) {
    this.setStringLit("http://gbol.life/0.1/mapName",val);
  }

  public void remParent(Organism val) {
    this.remRef("http://gbol.life/0.1/parent",val,true);
  }

  public List<? extends Organism> getAllParent() {
    return this.getRefSet("http://gbol.life/0.1/parent",true,Organism.class);
  }

  public void addParent(Organism val) {
    this.addRef("http://gbol.life/0.1/parent",val);
  }
}
