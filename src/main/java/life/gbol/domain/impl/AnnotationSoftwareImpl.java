package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationSoftware;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.impl.SoftwareAgentImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class AnnotationSoftwareImpl extends SoftwareAgentImpl implements AnnotationSoftware {
  public static final String TypeIRI = "http://gbol.life/0.1/AnnotationSoftware";

  protected AnnotationSoftwareImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static AnnotationSoftware make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationSoftwareImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,AnnotationSoftware.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,AnnotationSoftware.class,false);
          if(toRet == null) {
            toRet = new AnnotationSoftwareImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof AnnotationSoftware)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationSoftwareImpl expected");
        }
      }
      return (AnnotationSoftware)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://xmlns.com/foaf/0.1/name");
    this.checkCardMin1("http://gbol.life/0.1/version");
  }

  public String getName() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/name",val);
  }

  public String getCommitId() {
    return this.getStringLit("http://gbol.life/0.1/commitId",true);
  }

  public void setCommitId(String val) {
    this.setStringLit("http://gbol.life/0.1/commitId",val);
  }

  public String getVersion() {
    return this.getStringLit("http://gbol.life/0.1/version",false);
  }

  public void setVersion(String val) {
    this.setStringLit("http://gbol.life/0.1/version",val);
  }

  public String getCodeRepository() {
    return this.getExternalRef("http://gbol.life/0.1/codeRepository",true);
  }

  public void setCodeRepository(String val) {
    this.setExternalRef("http://gbol.life/0.1/codeRepository",val);
  }

  public String getHomepage() {
    return this.getStringLit("http://xmlns.com/foaf/0.1/homepage",true);
  }

  public void setHomepage(String val) {
    this.setStringLit("http://xmlns.com/foaf/0.1/homepage",val);
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
