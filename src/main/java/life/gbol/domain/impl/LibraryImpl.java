package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Library;
import life.gbol.domain.Provenance;
import life.gbol.domain.Sample;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class LibraryImpl extends OWLThingImpl implements Library {
  public static final String TypeIRI = "http://gbol.life/0.1/Library";

  protected LibraryImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Library make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new LibraryImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Library.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Library.class,false);
          if(toRet == null) {
            toRet = new LibraryImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Library)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.LibraryImpl expected");
        }
      }
      return (Library)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/accpetedSameBarcodeRatio");
    this.checkCardMin1("http://gbol.life/0.1/sample");
    this.checkCardMin1("http://gbol.life/0.1/libraryNum");
    this.checkCardMin1("http://gbol.life/0.1/barcodeHitsAccepted");
    this.checkCardMin1("http://gbol.life/0.1/barcodeHitsAcceptedRatio");
    this.checkCardMin1("http://gbol.life/0.1/primerHitsAcceptedRatio");
    this.checkCardMin1("http://gbol.life/0.1/provenance");
    this.checkCardMin1("http://gbol.life/0.1/fBarcodeLength");
    this.checkCardMin1("http://gbol.life/0.1/rFile");
    this.checkCardMin1("http://gbol.life/0.1/totalReads");
    this.checkCardMin1("http://gbol.life/0.1/primerHitsAccepted");
    this.checkCardMin1("http://gbol.life/0.1/fFile");
    this.checkCardMin1("http://gbol.life/0.1/rBarcodeLength");
  }

  public Float getAccpetedSameBarcodeRatio() {
    return this.getFloatLit("http://gbol.life/0.1/accpetedSameBarcodeRatio",false);
  }

  public void setAccpetedSameBarcodeRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/accpetedSameBarcodeRatio",val);
  }

  public void remSample(Sample val) {
    this.remRef("http://gbol.life/0.1/sample",val,false);
  }

  public List<? extends Sample> getAllSample() {
    return this.getRefSet("http://gbol.life/0.1/sample",false,Sample.class);
  }

  public void addSample(Sample val) {
    this.addRef("http://gbol.life/0.1/sample",val);
  }

  public Integer getLibraryNum() {
    return this.getIntegerLit("http://gbol.life/0.1/libraryNum",false);
  }

  public void setLibraryNum(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/libraryNum",val);
  }

  public Integer getBarcodeHitsAccepted() {
    return this.getIntegerLit("http://gbol.life/0.1/barcodeHitsAccepted",false);
  }

  public void setBarcodeHitsAccepted(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/barcodeHitsAccepted",val);
  }

  public String getFBarcodeFile() {
    return this.getStringLit("http://gbol.life/0.1/fBarcodeFile",true);
  }

  public void setFBarcodeFile(String val) {
    this.setStringLit("http://gbol.life/0.1/fBarcodeFile",val);
  }

  public Float getBarcodeHitsAcceptedRatio() {
    return this.getFloatLit("http://gbol.life/0.1/barcodeHitsAcceptedRatio",false);
  }

  public void setBarcodeHitsAcceptedRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/barcodeHitsAcceptedRatio",val);
  }

  public Float getPrimerHitsAcceptedRatio() {
    return this.getFloatLit("http://gbol.life/0.1/primerHitsAcceptedRatio",false);
  }

  public void setPrimerHitsAcceptedRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/primerHitsAcceptedRatio",val);
  }

  public Provenance getProvenance() {
    return this.getRef("http://gbol.life/0.1/provenance",false,Provenance.class);
  }

  public void setProvenance(Provenance val) {
    this.setRef("http://gbol.life/0.1/provenance",val,Provenance.class);
  }

  public String getRBarcodeFile() {
    return this.getStringLit("http://gbol.life/0.1/rBarcodeFile",true);
  }

  public void setRBarcodeFile(String val) {
    this.setStringLit("http://gbol.life/0.1/rBarcodeFile",val);
  }

  public Integer getFBarcodeLength() {
    return this.getIntegerLit("http://gbol.life/0.1/fBarcodeLength",false);
  }

  public void setFBarcodeLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/fBarcodeLength",val);
  }

  public String getRFile() {
    return this.getStringLit("http://gbol.life/0.1/rFile",false);
  }

  public void setRFile(String val) {
    this.setStringLit("http://gbol.life/0.1/rFile",val);
  }

  public Integer getTotalReads() {
    return this.getIntegerLit("http://gbol.life/0.1/totalReads",false);
  }

  public void setTotalReads(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/totalReads",val);
  }

  public Integer getPrimerHitsAccepted() {
    return this.getIntegerLit("http://gbol.life/0.1/primerHitsAccepted",false);
  }

  public void setPrimerHitsAccepted(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/primerHitsAccepted",val);
  }

  public String getFFile() {
    return this.getStringLit("http://gbol.life/0.1/fFile",false);
  }

  public void setFFile(String val) {
    this.setStringLit("http://gbol.life/0.1/fFile",val);
  }

  public Integer getRBarcodeLength() {
    return this.getIntegerLit("http://gbol.life/0.1/rBarcodeLength",false);
  }

  public void setRBarcodeLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/rBarcodeLength",val);
  }
}
