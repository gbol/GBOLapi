package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.Note;
import life.gbol.domain.Organism;
import life.gbol.domain.Protein;
import life.gbol.domain.ProteinFeature;
import life.gbol.domain.Sample;
import life.gbol.domain.SequenceAssembly;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProteinImpl extends SequenceImpl implements Protein {
  public static final String TypeIRI = "http://gbol.life/0.1/Protein";

  protected ProteinImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Protein make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProteinImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Protein.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Protein.class,false);
          if(toRet == null) {
            toRet = new ProteinImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Protein)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinImpl expected");
        }
      }
      return (Protein)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public void remFeature(ProteinFeature val) {
    this.remRef("http://gbol.life/0.1/feature",val,true);
  }

  public List<? extends ProteinFeature> getAllFeature() {
    return this.getRefSet("http://gbol.life/0.1/feature",true,ProteinFeature.class);
  }

  public void addFeature(ProteinFeature val) {
    this.addRef("http://gbol.life/0.1/feature",val);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public Sample getSample() {
    return this.getRef("http://gbol.life/0.1/sample",true,Sample.class);
  }

  public void setSample(Sample val) {
    this.setRef("http://gbol.life/0.1/sample",val,Sample.class);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life/0.1/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life/0.1/description",val);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public Integer getSequenceVersion() {
    return this.getIntegerLit("http://gbol.life/0.1/sequenceVersion",true);
  }

  public void setSequenceVersion(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/sequenceVersion",val);
  }

  public String getSequence() {
    return this.getStringLit("http://gbol.life/0.1/sequence",false);
  }

  public void setSequence(String val) {
    this.setStringLit("http://gbol.life/0.1/sequence",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public Long getLength() {
    return this.getLongLit("http://gbol.life/0.1/length",false);
  }

  public void setLength(Long val) {
    this.setLongLit("http://gbol.life/0.1/length",val);
  }

  public void remAlternativeNames(String val) {
    this.remStringLit("http://gbol.life/0.1/alternativeNames",val,true);
  }

  public List<? extends String> getAllAlternativeNames() {
    return this.getStringLitSet("http://gbol.life/0.1/alternativeNames",true);
  }

  public void addAlternativeNames(String val) {
    this.addStringLit("http://gbol.life/0.1/alternativeNames",val);
  }

  public Organism getOrganism() {
    return this.getRef("http://gbol.life/0.1/organism",true,Organism.class);
  }

  public void setOrganism(Organism val) {
    this.setRef("http://gbol.life/0.1/organism",val,Organism.class);
  }

  public String getRecommendedName() {
    return this.getStringLit("http://gbol.life/0.1/recommendedName",true);
  }

  public void setRecommendedName(String val) {
    this.setStringLit("http://gbol.life/0.1/recommendedName",val);
  }

  public String getCommonName() {
    return this.getStringLit("http://gbol.life/0.1/commonName",true);
  }

  public void setCommonName(String val) {
    this.setStringLit("http://gbol.life/0.1/commonName",val);
  }

  public String getSha384() {
    return this.getStringLit("http://gbol.life/0.1/sha384",false);
  }

  public void setSha384(String val) {
    this.setStringLit("http://gbol.life/0.1/sha384",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public String getShortName() {
    return this.getStringLit("http://gbol.life/0.1/shortName",true);
  }

  public void setShortName(String val) {
    this.setStringLit("http://gbol.life/0.1/shortName",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }

  public SequenceAssembly getAssembly() {
    return this.getRef("http://gbol.life/0.1/assembly",true,SequenceAssembly.class);
  }

  public void setAssembly(SequenceAssembly val) {
    this.setRef("http://gbol.life/0.1/assembly",val,SequenceAssembly.class);
  }
}
