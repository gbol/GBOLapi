package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProvenanceClassification;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceClassificationImpl extends ProvenanceApplicationImpl implements ProvenanceClassification {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceClassification";

  protected ProvenanceClassificationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceClassification make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceClassificationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceClassification.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceClassification.class,false);
          if(toRet == null) {
            toRet = new ProvenanceClassificationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceClassification)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceClassificationImpl expected");
        }
      }
      return (ProvenanceClassification)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
