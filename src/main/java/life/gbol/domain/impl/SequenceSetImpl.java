package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class SequenceSetImpl extends OWLThingImpl implements SequenceSet {
  public static final String TypeIRI = "http://gbol.life/0.1/SequenceSet";

  protected SequenceSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SequenceSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SequenceSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SequenceSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SequenceSet.class,false);
          if(toRet == null) {
            toRet = new SequenceSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SequenceSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceSetImpl expected");
        }
      }
      return (SequenceSet)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
