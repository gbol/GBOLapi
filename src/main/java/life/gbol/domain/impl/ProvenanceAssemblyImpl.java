package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProvenanceAssembly;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ProvenanceAssemblyImpl extends ProvenanceApplicationImpl implements ProvenanceAssembly {
  public static final String TypeIRI = "http://gbol.life/0.1/ProvenanceAssembly";

  protected ProvenanceAssemblyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ProvenanceAssembly make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ProvenanceAssemblyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ProvenanceAssembly.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ProvenanceAssembly.class,false);
          if(toRet == null) {
            toRet = new ProvenanceAssemblyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ProvenanceAssembly)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceAssemblyImpl expected");
        }
      }
      return (ProvenanceAssembly)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
