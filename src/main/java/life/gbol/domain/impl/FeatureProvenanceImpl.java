package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.ProvenanceApplication;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class FeatureProvenanceImpl extends ProvenanceImpl implements FeatureProvenance {
  public static final String TypeIRI = "http://gbol.life/0.1/FeatureProvenance";

  protected FeatureProvenanceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static FeatureProvenance make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new FeatureProvenanceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,FeatureProvenance.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,FeatureProvenance.class,false);
          if(toRet == null) {
            toRet = new FeatureProvenanceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof FeatureProvenance)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.FeatureProvenanceImpl expected");
        }
      }
      return (FeatureProvenance)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public void remOnProperty(String val) {
    this.remExternalRef("http://gbol.life/0.1/onProperty",val,true);
  }

  public List<? extends String> getAllOnProperty() {
    return this.getExternalRefSet("http://gbol.life/0.1/onProperty",true);
  }

  public void addOnProperty(String val) {
    this.addExternalRef("http://gbol.life/0.1/onProperty",val);
  }

  public AnnotationResult getOrigin() {
    return this.getRef("http://gbol.life/0.1/origin",false,AnnotationResult.class);
  }

  public void setOrigin(AnnotationResult val) {
    this.setRef("http://gbol.life/0.1/origin",val,AnnotationResult.class);
  }

  public ProvenanceApplication getAnnotation() {
    return this.getRef("http://gbol.life/0.1/annotation",true,ProvenanceApplication.class);
  }

  public void setAnnotation(ProvenanceApplication val) {
    this.setRef("http://gbol.life/0.1/annotation",val,ProvenanceApplication.class);
  }
}
