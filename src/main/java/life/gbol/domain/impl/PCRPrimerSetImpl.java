package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PCRPrimer;
import life.gbol.domain.PCRPrimerSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PCRPrimerSetImpl extends SequenceSetImpl implements PCRPrimerSet {
  public static final String TypeIRI = "http://gbol.life/0.1/PCRPrimerSet";

  protected PCRPrimerSetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PCRPrimerSet make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PCRPrimerSetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PCRPrimerSet.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PCRPrimerSet.class,false);
          if(toRet == null) {
            toRet = new PCRPrimerSetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PCRPrimerSet)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PCRPrimerSetImpl expected");
        }
      }
      return (PCRPrimerSet)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/forwardPrimer");
  }

  public PCRPrimer getReversePrimer() {
    return this.getRef("http://gbol.life/0.1/reversePrimer",true,PCRPrimer.class);
  }

  public void setReversePrimer(PCRPrimer val) {
    this.setRef("http://gbol.life/0.1/reversePrimer",val,PCRPrimer.class);
  }

  public PCRPrimer getForwardPrimer() {
    return this.getRef("http://gbol.life/0.1/forwardPrimer",false,PCRPrimer.class);
  }

  public void setForwardPrimer(PCRPrimer val) {
    this.setRef("http://gbol.life/0.1/forwardPrimer",val,PCRPrimer.class);
  }
}
