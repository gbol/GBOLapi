package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.BagOfRegions;
import life.gbol.domain.Region;
import life.gbol.domain.Sequence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class BagOfRegionsImpl extends CollectionOfRegionsImpl implements BagOfRegions {
  public static final String TypeIRI = "http://gbol.life/0.1/BagOfRegions";

  protected BagOfRegionsImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static BagOfRegions make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new BagOfRegionsImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,BagOfRegions.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,BagOfRegions.class,false);
          if(toRet == null) {
            toRet = new BagOfRegionsImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof BagOfRegions)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.BagOfRegionsImpl expected");
        }
      }
      return (BagOfRegions)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/members");
  }

  public void remMembers(Region val) {
    this.remRef("http://gbol.life/0.1/members",val,false);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefSet("http://gbol.life/0.1/members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRef("http://gbol.life/0.1/members",val);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
