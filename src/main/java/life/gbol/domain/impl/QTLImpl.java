package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.Feature;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.Organism;
import life.gbol.domain.QTL;
import life.gbol.domain.QTLMap;
import life.gbol.domain.QTLTypes;
import life.gbol.domain.ReasonArtificialLocation;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class QTLImpl extends FeatureImpl implements QTL {
  public static final String TypeIRI = "http://gbol.life/0.1/QTL";

  protected QTLImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static QTL make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new QTLImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,QTL.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,QTL.class,false);
          if(toRet == null) {
            toRet = new QTLImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof QTL)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.QTLImpl expected");
        }
      }
      return (QTL)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/Type");
  }

  public String getComment() {
    return this.getStringLit("http://gbol.life/0.1/comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://gbol.life/0.1/comment",val);
  }

  public QTLMap getQtlMap() {
    return this.getRef("http://gbol.life/0.1/qtlMap",true,QTLMap.class);
  }

  public void setQtlMap(QTLMap val) {
    this.setRef("http://gbol.life/0.1/qtlMap",val,QTLMap.class);
  }

  public void remTrait(String val) {
    this.remExternalRef("http://gbol.life/0.1/trait",val,true);
  }

  public List<? extends String> getAllTrait() {
    return this.getExternalRefSet("http://gbol.life/0.1/trait",true);
  }

  public void addTrait(String val) {
    this.addExternalRef("http://gbol.life/0.1/trait",val);
  }

  public void remAssociatedFeature(Feature val) {
    this.remRef("http://gbol.life/0.1/associatedFeature",val,true);
  }

  public List<? extends Feature> getAllAssociatedFeature() {
    return this.getRefSet("http://gbol.life/0.1/associatedFeature",true,Feature.class);
  }

  public void addAssociatedFeature(Feature val) {
    this.addRef("http://gbol.life/0.1/associatedFeature",val);
  }

  public String getSymbol() {
    return this.getStringLit("http://gbol.life/0.1/symbol",true);
  }

  public void setSymbol(String val) {
    this.setStringLit("http://gbol.life/0.1/symbol",val);
  }

  public Double getRelativeStart() {
    return this.getDoubleLit("http://gbol.life/0.1/relativeStart",true);
  }

  public void setRelativeStart(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/relativeStart",val);
  }

  public Double getRelativeEnd() {
    return this.getDoubleLit("http://gbol.life/0.1/relativeEnd",true);
  }

  public void setRelativeEnd(Double val) {
    this.setDoubleLit("http://gbol.life/0.1/relativeEnd",val);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life/0.1/accession",true);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life/0.1/accession",val);
  }

  public void remParent(Organism val) {
    this.remRef("http://gbol.life/0.1/parent",val,true);
  }

  public List<? extends Organism> getAllParent() {
    return this.getRefSet("http://gbol.life/0.1/parent",true,Organism.class);
  }

  public void addParent(Organism val) {
    this.addRef("http://gbol.life/0.1/parent",val);
  }

  public String getTraitName() {
    return this.getStringLit("http://gbol.life/0.1/traitName",true);
  }

  public void setTraitName(String val) {
    this.setStringLit("http://gbol.life/0.1/traitName",val);
  }

  public void remTerm(String val) {
    this.remStringLit("http://gbol.life/0.1/term",val,true);
  }

  public List<? extends String> getAllTerm() {
    return this.getStringLitSet("http://gbol.life/0.1/term",true);
  }

  public void addTerm(String val) {
    this.addStringLit("http://gbol.life/0.1/term",val);
  }

  public QTLTypes getType() {
    return this.getEnum("http://gbol.life/0.1/Type",false,QTLTypes.class);
  }

  public void setType(QTLTypes val) {
    this.setEnum("http://gbol.life/0.1/Type",val,QTLTypes.class);
  }

  public String getLinkageGroup() {
    return this.getStringLit("http://gbol.life/0.1/linkageGroup",true);
  }

  public void setLinkageGroup(String val) {
    this.setStringLit("http://gbol.life/0.1/linkageGroup",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life/0.1/location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life/0.1/location",val,Location.class);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getPhenotype() {
    return this.getStringLit("http://gbol.life/0.1/phenotype",true);
  }

  public void setPhenotype(String val) {
    this.setStringLit("http://gbol.life/0.1/phenotype",val);
  }

  public void remProvenance(FeatureProvenance val) {
    this.remRef("http://gbol.life/0.1/provenance",val,false);
  }

  public List<? extends FeatureProvenance> getAllProvenance() {
    return this.getRefSet("http://gbol.life/0.1/provenance",false,FeatureProvenance.class);
  }

  public void addProvenance(FeatureProvenance val) {
    this.addRef("http://gbol.life/0.1/provenance",val);
  }

  public ReasonArtificialLocation getArtificialLocation() {
    return this.getEnum("http://gbol.life/0.1/artificialLocation",true,ReasonArtificialLocation.class);
  }

  public void setArtificialLocation(ReasonArtificialLocation val) {
    this.setEnum("http://gbol.life/0.1/artificialLocation",val,ReasonArtificialLocation.class);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }
}
