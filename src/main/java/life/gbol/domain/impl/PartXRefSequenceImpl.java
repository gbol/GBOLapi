package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PartXRefSequence;
import life.gbol.domain.Region;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class PartXRefSequenceImpl extends PartSequenceImpl implements PartXRefSequence {
  public static final String TypeIRI = "http://gbol.life/0.1/PartXRefSequence";

  protected PartXRefSequenceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PartXRefSequence make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PartXRefSequenceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PartXRefSequence.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PartXRefSequence.class,false);
          if(toRet == null) {
            toRet = new PartXRefSequenceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PartXRefSequence)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.PartXRefSequenceImpl expected");
        }
      }
      return (PartXRefSequence)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/includedXRefSequence");
  }

  public String getIncludedXRefVersion() {
    return this.getStringLit("http://gbol.life/0.1/includedXRefVersion",true);
  }

  public void setIncludedXRefVersion(String val) {
    this.setStringLit("http://gbol.life/0.1/includedXRefVersion",val);
  }

  public XRef getIncludedXRefSequence() {
    return this.getRef("http://gbol.life/0.1/includedXRefSequence",false,XRef.class);
  }

  public void setIncludedXRefSequence(XRef val) {
    this.setRef("http://gbol.life/0.1/includedXRefSequence",val,XRef.class);
  }

  public Region getIncludedRegion() {
    return this.getRef("http://gbol.life/0.1/includedRegion",false,Region.class);
  }

  public void setIncludedRegion(Region val) {
    this.setRef("http://gbol.life/0.1/includedRegion",val,Region.class);
  }
}
