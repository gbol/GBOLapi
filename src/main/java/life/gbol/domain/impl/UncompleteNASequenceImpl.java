package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Citation;
import life.gbol.domain.Expression;
import life.gbol.domain.NAFeature;
import life.gbol.domain.NASequence;
import life.gbol.domain.Note;
import life.gbol.domain.Organism;
import life.gbol.domain.Sample;
import life.gbol.domain.SequenceAssembly;
import life.gbol.domain.StrandType;
import life.gbol.domain.Topology;
import life.gbol.domain.UncompleteNASequence;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class UncompleteNASequenceImpl extends NASequenceImpl implements UncompleteNASequence {
  public static final String TypeIRI = "http://gbol.life/0.1/UncompleteNASequence";

  protected UncompleteNASequenceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static UncompleteNASequence make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new UncompleteNASequenceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,UncompleteNASequence.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,UncompleteNASequence.class,false);
          if(toRet == null) {
            toRet = new UncompleteNASequenceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof UncompleteNASequence)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.UncompleteNASequenceImpl expected");
        }
      }
      return (UncompleteNASequence)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/strandType");
  }

  public StrandType getStrandType() {
    return this.getEnum("http://gbol.life/0.1/strandType",false,StrandType.class);
  }

  public void setStrandType(StrandType val) {
    this.setEnum("http://gbol.life/0.1/strandType",val,StrandType.class);
  }

  public Topology getTopology() {
    return this.getEnum("http://gbol.life/0.1/topology",true,Topology.class);
  }

  public void setTopology(Topology val) {
    this.setEnum("http://gbol.life/0.1/topology",val,Topology.class);
  }

  public void remClone(String val) {
    this.remStringLit("http://gbol.life/0.1/clone",val,true);
  }

  public List<? extends String> getAllClone() {
    return this.getStringLitSet("http://gbol.life/0.1/clone",true);
  }

  public void addClone(String val) {
    this.addStringLit("http://gbol.life/0.1/clone",val);
  }

  public void remExpression(Expression val) {
    this.remRef("http://gbol.life/0.1/expression",val,true);
  }

  public List<? extends Expression> getAllExpression() {
    return this.getRefSet("http://gbol.life/0.1/expression",true,Expression.class);
  }

  public void addExpression(Expression val) {
    this.addRef("http://gbol.life/0.1/expression",val);
  }

  public Boolean getRearranged() {
    return this.getBooleanLit("http://gbol.life/0.1/rearranged",true);
  }

  public void setRearranged(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/rearranged",val);
  }

  public Integer getTranslTable() {
    return this.getIntegerLit("http://gbol.life/0.1/translTable",true);
  }

  public void setTranslTable(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/translTable",val);
  }

  public Boolean getMacronuclear() {
    return this.getBooleanLit("http://gbol.life/0.1/macronuclear",true);
  }

  public void setMacronuclear(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/macronuclear",val);
  }

  public void remFeature(NAFeature val) {
    this.remRef("http://gbol.life/0.1/feature",val,true);
  }

  public List<? extends NAFeature> getAllFeature() {
    return this.getRefSet("http://gbol.life/0.1/feature",true,NAFeature.class);
  }

  public void addFeature(NAFeature val) {
    this.addRef("http://gbol.life/0.1/feature",val);
  }

  public NASequence getIntegratedInto() {
    return this.getRef("http://gbol.life/0.1/integratedInto",true,NASequence.class);
  }

  public void setIntegratedInto(NASequence val) {
    this.setRef("http://gbol.life/0.1/integratedInto",val,NASequence.class);
  }

  public Boolean getProviral() {
    return this.getBooleanLit("http://gbol.life/0.1/proviral",true);
  }

  public void setProviral(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/proviral",val);
  }

  public String getCloneLib() {
    return this.getStringLit("http://gbol.life/0.1/cloneLib",true);
  }

  public void setCloneLib(String val) {
    this.setStringLit("http://gbol.life/0.1/cloneLib",val);
  }

  public void remCitation(Citation val) {
    this.remRef("http://gbol.life/0.1/citation",val,true);
  }

  public List<? extends Citation> getAllCitation() {
    return this.getRefSet("http://gbol.life/0.1/citation",true,Citation.class);
  }

  public void addCitation(Citation val) {
    this.addRef("http://gbol.life/0.1/citation",val);
  }

  public Sample getSample() {
    return this.getRef("http://gbol.life/0.1/sample",true,Sample.class);
  }

  public void setSample(Sample val) {
    this.setRef("http://gbol.life/0.1/sample",val,Sample.class);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life/0.1/function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life/0.1/function",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life/0.1/description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life/0.1/description",val);
  }

  public void remAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/accession",val,true);
  }

  public List<? extends String> getAllAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/accession",true);
  }

  public void addAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/accession",val);
  }

  public Integer getSequenceVersion() {
    return this.getIntegerLit("http://gbol.life/0.1/sequenceVersion",true);
  }

  public void setSequenceVersion(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/sequenceVersion",val);
  }

  public String getSequence() {
    return this.getStringLit("http://gbol.life/0.1/sequence",false);
  }

  public void setSequence(String val) {
    this.setStringLit("http://gbol.life/0.1/sequence",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life/0.1/xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life/0.1/xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life/0.1/xref",val);
  }

  public Long getLength() {
    return this.getLongLit("http://gbol.life/0.1/length",false);
  }

  public void setLength(Long val) {
    this.setLongLit("http://gbol.life/0.1/length",val);
  }

  public void remAlternativeNames(String val) {
    this.remStringLit("http://gbol.life/0.1/alternativeNames",val,true);
  }

  public List<? extends String> getAllAlternativeNames() {
    return this.getStringLitSet("http://gbol.life/0.1/alternativeNames",true);
  }

  public void addAlternativeNames(String val) {
    this.addStringLit("http://gbol.life/0.1/alternativeNames",val);
  }

  public Organism getOrganism() {
    return this.getRef("http://gbol.life/0.1/organism",true,Organism.class);
  }

  public void setOrganism(Organism val) {
    this.setRef("http://gbol.life/0.1/organism",val,Organism.class);
  }

  public String getRecommendedName() {
    return this.getStringLit("http://gbol.life/0.1/recommendedName",true);
  }

  public void setRecommendedName(String val) {
    this.setStringLit("http://gbol.life/0.1/recommendedName",val);
  }

  public String getCommonName() {
    return this.getStringLit("http://gbol.life/0.1/commonName",true);
  }

  public void setCommonName(String val) {
    this.setStringLit("http://gbol.life/0.1/commonName",val);
  }

  public String getSha384() {
    return this.getStringLit("http://gbol.life/0.1/sha384",false);
  }

  public void setSha384(String val) {
    this.setStringLit("http://gbol.life/0.1/sha384",val);
  }

  public String getStandardName() {
    return this.getStringLit("http://gbol.life/0.1/standardName",true);
  }

  public void setStandardName(String val) {
    this.setStringLit("http://gbol.life/0.1/standardName",val);
  }

  public String getShortName() {
    return this.getStringLit("http://gbol.life/0.1/shortName",true);
  }

  public void setShortName(String val) {
    this.setStringLit("http://gbol.life/0.1/shortName",val);
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life/0.1/note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life/0.1/note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life/0.1/note",val);
  }

  public SequenceAssembly getAssembly() {
    return this.getRef("http://gbol.life/0.1/assembly",true,SequenceAssembly.class);
  }

  public void setAssembly(SequenceAssembly val) {
    this.setRef("http://gbol.life/0.1/assembly",val,SequenceAssembly.class);
  }
}
