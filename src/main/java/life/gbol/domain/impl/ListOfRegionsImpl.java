package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.ListOfRegions;
import life.gbol.domain.Region;
import life.gbol.domain.Sequence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class ListOfRegionsImpl extends CollectionOfRegionsImpl implements ListOfRegions {
  public static final String TypeIRI = "http://gbol.life/0.1/ListOfRegions";

  protected ListOfRegionsImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ListOfRegions make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ListOfRegionsImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ListOfRegions.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ListOfRegions.class,false);
          if(toRet == null) {
            toRet = new ListOfRegionsImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ListOfRegions)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.ListOfRegionsImpl expected");
        }
      }
      return (ListOfRegions)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/members");
  }

  public Region getMembers(int index) {
    return this.getRefListAtIndex("http://gbol.life/0.1/members",false,Region.class,index);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefList("http://gbol.life/0.1/members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRefList("http://gbol.life/0.1/members",val);
  }

  public void setMembers(Region val, int index) {
    this.setRefList("http://gbol.life/0.1/members",val,false,index);
  }

  public void remMembers(Region val) {
    this.remRefList("http://gbol.life/0.1/members",val,false);
  }

  public Sequence getReferenceSequence() {
    return this.getRef("http://gbol.life/0.1/referenceSequence",true,Sequence.class);
  }

  public void setReferenceSequence(Sequence val) {
    this.setRef("http://gbol.life/0.1/referenceSequence",val,Sequence.class);
  }
}
