package life.gbol.domain;

import com.xmlns.foaf.domain.Agent;
import java.lang.Boolean;
import java.lang.Double;
import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Sample extends OWLThing {
  Boolean getProviralExtraction();

  void setProviralExtraction(Boolean val);

  String getMap();

  void setMap(String val);

  void remMetadata(String val);

  List<? extends String> getAllMetadata();

  void addMetadata(String val);

  String getStrain();

  void setStrain(String val);

  String getEcotype();

  void setEcotype(String val);

  String getSubClone();

  void setSubClone(String val);

  String getIsolate();

  void setIsolate(String val);

  String getHaplogroup();

  void setHaplogroup(String val);

  String getDevStage();

  void setDevStage(String val);

  String getSex();

  void setSex(String val);

  String getPlasmid();

  void setPlasmid(String val);

  BarcodeSet getBarcodes();

  void setBarcodes(BarcodeSet val);

  String getPopVariant();

  void setPopVariant(String val);

  String getHaplotype();

  void setHaplotype(String val);

  String getName();

  void setName(String val);

  String getIsolationSource();

  void setIsolationSource(String val);

  String getLabHost();

  void setLabHost(String val);

  GeographicalCoordinate getGeographicalCoordinate();

  void setGeographicalCoordinate(GeographicalCoordinate val);

  String getTissueLib();

  void setTissueLib(String val);

  String getOrganelle();

  void setOrganelle(String val);

  String getCellType();

  void setCellType(String val);

  String getSubSpecies();

  void setSubSpecies(String val);

  Double getAltitude();

  void setAltitude(Double val);

  String getCellLine();

  void setCellLine(String val);

  String getTissueType();

  void setTissueType(String val);

  String getCultivar();

  void setCultivar(String val);

  String getMatingType();

  void setMatingType(String val);

  void remCitation(Citation val);

  List<? extends Citation> getAllCitation();

  void addCitation(Citation val);

  void remSourceMaterial(MaterialSource val);

  List<? extends MaterialSource> getAllSourceMaterial();

  void addSourceMaterial(MaterialSource val);

  String getSubStrain();

  void setSubStrain(String val);

  String getDescription();

  void setDescription(String val);

  String getChromosome();

  void setChromosome(String val);

  void remCollectedBy(Agent val);

  List<? extends Agent> getAllCollectedBy();

  void addCollectedBy(Agent val);

  Agent getIdentifiedBy();

  void setIdentifiedBy(Agent val);

  void remAsv(ASVSet val);

  List<? extends ASVSet> getAllAsv();

  void addAsv(ASVSet val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  String getVariety();

  void setVariety(String val);

  TaxonomyRef getHost();

  void setHost(TaxonomyRef val);

  PCRPrimerSet getPCRPrimers();

  void setPCRPrimers(PCRPrimerSet val);

  LocalDateTime getCollectionDate();

  void setCollectionDate(LocalDateTime val);

  GeographicalLocation getGeographicalLocation();

  void setGeographicalLocation(GeographicalLocation val);

  String getSegment();

  void setSegment(String val);

  Boolean getEnvironmentalSample();

  void setEnvironmentalSample(Boolean val);

  String getSerotype();

  void setSerotype(String val);

  void remNote(Note val);

  List<? extends Note> getAllNote();

  void addNote(Note val);
}
