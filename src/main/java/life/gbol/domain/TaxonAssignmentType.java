package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum TaxonAssignmentType implements EnumClass {
  HitsTaxon("http://gbol.life/0.1/HitsTaxon",new TaxonAssignmentType[]{}),

  AssignedTaxon("http://gbol.life/0.1/AssignedTaxon",new TaxonAssignmentType[]{}),

  BestTaxon("http://gbol.life/0.1/BestTaxon",new TaxonAssignmentType[]{});

  private TaxonAssignmentType[] parents;

  private String iri;

  private TaxonAssignmentType(String iri, TaxonAssignmentType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static TaxonAssignmentType make(String iri) {
    for(TaxonAssignmentType item : TaxonAssignmentType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
