package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Interleaved extends ReadInformation {
  Integer getInsertSize();

  void setInsertSize(Integer val);
}
