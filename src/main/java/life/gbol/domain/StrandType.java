package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum StrandType implements EnumClass {
  SingleStrandedRNA("http://gbol.life/0.1/SingleStrandedRNA",new StrandType[]{}),

  DoubleStrandedDNA("http://gbol.life/0.1/DoubleStrandedDNA",new StrandType[]{}),

  ComplementaryDNA("http://gbol.life/0.1/ComplementaryDNA",new StrandType[]{DoubleStrandedDNA}),

  SingleStrandedDNA("http://gbol.life/0.1/SingleStrandedDNA",new StrandType[]{}),

  DoubleStrandedRNA("http://gbol.life/0.1/DoubleStrandedRNA",new StrandType[]{});

  private StrandType[] parents;

  private String iri;

  private StrandType(String iri, StrandType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static StrandType make(String iri) {
    for(StrandType item : StrandType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
