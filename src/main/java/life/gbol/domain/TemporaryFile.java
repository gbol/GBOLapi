package life.gbol.domain;

import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface TemporaryFile extends Entity {
  Entity getWasDerivedFrom();

  void setWasDerivedFrom(Entity val);
}
