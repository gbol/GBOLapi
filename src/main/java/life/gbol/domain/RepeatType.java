package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum RepeatType implements EnumClass {
  Direct("http://gbol.life/0.1/Direct",new RepeatType[]{}),

  Terminal("http://gbol.life/0.1/Terminal",new RepeatType[]{}),

  CentromericRepeat("http://gbol.life/0.1/CentromericRepeat",new RepeatType[]{}),

  Dispersed("http://gbol.life/0.1/Dispersed",new RepeatType[]{}),

  Tandem("http://gbol.life/0.1/Tandem",new RepeatType[]{}),

  TelomericRepeat("http://gbol.life/0.1/TelomericRepeat",new RepeatType[]{}),

  YPrimeElement("http://gbol.life/0.1/YPrimeElement",new RepeatType[]{}),

  Nested("http://gbol.life/0.1/Nested",new RepeatType[]{}),

  NonLtrRetrotransposonPolymericTract("http://gbol.life/0.1/NonLtrRetrotransposonPolymericTract",new RepeatType[]{}),

  CRISPRRepeat("http://gbol.life/0.1/CRISPRRepeat",new RepeatType[]{}),

  Inverted("http://gbol.life/0.1/Inverted",new RepeatType[]{}),

  LongTerminalRepeat("http://gbol.life/0.1/LongTerminalRepeat",new RepeatType[]{}),

  RepeatTypeOther("http://gbol.life/0.1/RepeatTypeOther",new RepeatType[]{}),

  XElementCombinatorialRepeat("http://gbol.life/0.1/XElementCombinatorialRepeat",new RepeatType[]{}),

  Flanking("http://gbol.life/0.1/Flanking",new RepeatType[]{});

  private RepeatType[] parents;

  private String iri;

  private RepeatType(String iri, RepeatType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static RepeatType make(String iri) {
    for(RepeatType item : RepeatType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
