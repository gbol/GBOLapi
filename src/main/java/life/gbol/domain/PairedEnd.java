package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PairedEnd extends ReadInformation {
  LocalSequenceFile getFile2();

  void setFile2(LocalSequenceFile val);

  Integer getInsertSize();

  void setInsertSize(Integer val);
}
