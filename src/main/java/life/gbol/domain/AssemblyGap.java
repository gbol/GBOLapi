package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface AssemblyGap extends AssemblyAnnotation {
  LinkageEvidence getLinkageEvidence();

  void setLinkageEvidence(LinkageEvidence val);

  GapType getGapType();

  void setGapType(GapType val);
}
