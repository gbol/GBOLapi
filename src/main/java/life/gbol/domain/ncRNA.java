package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ncRNA extends MaturedRNA {
  String getProduct();

  void setProduct(String val);

  ncRNAType getNcRNAClass();

  void setNcRNAClass(ncRNAType val);
}
