package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MoietyBindingSite extends BindingSite {
  String getBindingMoiety();

  void setBindingMoiety(String val);
}
