package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum CollectionSampleType implements EnumClass {
  SpecimenVoucher("http://gbol.life/0.1/SpecimenVoucher",new CollectionSampleType[]{}),

  CultureCollection("http://gbol.life/0.1/CultureCollection",new CollectionSampleType[]{}),

  OtherCollection("http://gbol.life/0.1/OtherCollection",new CollectionSampleType[]{});

  private CollectionSampleType[] parents;

  private String iri;

  private CollectionSampleType(String iri, CollectionSampleType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static CollectionSampleType make(String iri) {
    for(CollectionSampleType item : CollectionSampleType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
