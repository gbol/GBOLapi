package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum LinkageEvidence implements EnumClass {
  LinkageNA("http://gbol.life/0.1/LinkageNA",new LinkageEvidence[]{}),

  AlignGenus("http://gbol.life/0.1/AlignGenus",new LinkageEvidence[]{}),

  PCR("http://gbol.life/0.1/PCR",new LinkageEvidence[]{}),

  Map("http://gbol.life/0.1/Map",new LinkageEvidence[]{}),

  AlignXGenus("http://gbol.life/0.1/AlignXGenus",new LinkageEvidence[]{}),

  Strobe("http://gbol.life/0.1/Strobe",new LinkageEvidence[]{}),

  Unspecified("http://gbol.life/0.1/Unspecified",new LinkageEvidence[]{}),

  PairedEnds("http://gbol.life/0.1/PairedEnds",new LinkageEvidence[]{}),

  WithinClone("http://gbol.life/0.1/WithinClone",new LinkageEvidence[]{}),

  AlignTranscript("http://gbol.life/0.1/AlignTranscript",new LinkageEvidence[]{}),

  CloneContig("http://gbol.life/0.1/CloneContig",new LinkageEvidence[]{});

  private LinkageEvidence[] parents;

  private String iri;

  private LinkageEvidence(String iri, LinkageEvidence[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static LinkageEvidence make(String iri) {
    for(LinkageEvidence item : LinkageEvidence.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
