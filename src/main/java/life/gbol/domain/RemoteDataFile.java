package life.gbol.domain;

import java.lang.Long;
import java.lang.String;
import java.time.LocalDateTime;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface RemoteDataFile extends DataFile {
  String getRemoteFileName();

  void setRemoteFileName(String val);

  LocalDateTime getRetrievedOn();

  void setRetrievedOn(LocalDateTime val);

  String getMd5();

  void setMd5(String val);

  FileType getFileType();

  void setFileType(FileType val);

  Long getSize();

  void setSize(Long val);

  String getRetrievedFrom();

  void setRetrievedFrom(String val);

  String getRemoteFilePath();

  void setRemoteFilePath(String val);
}
