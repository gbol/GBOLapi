package life.gbol.domain;

import java.lang.Integer;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Sequence extends OWLThing {
  void remCitation(Citation val);

  List<? extends Citation> getAllCitation();

  void addCitation(Citation val);

  Sample getSample();

  void setSample(Sample val);

  String getFunction();

  void setFunction(String val);

  String getDescription();

  void setDescription(String val);

  void remAccession(String val);

  List<? extends String> getAllAccession();

  void addAccession(String val);

  Integer getSequenceVersion();

  void setSequenceVersion(Integer val);

  String getSequence();

  void setSequence(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  Long getLength();

  void setLength(Long val);

  void remAlternativeNames(String val);

  List<? extends String> getAllAlternativeNames();

  void addAlternativeNames(String val);

  void remFeature(Feature val);

  List<? extends Feature> getAllFeature();

  void addFeature(Feature val);

  Organism getOrganism();

  void setOrganism(Organism val);

  String getRecommendedName();

  void setRecommendedName(String val);

  String getCommonName();

  void setCommonName(String val);

  String getSha384();

  void setSha384(String val);

  String getStandardName();

  void setStandardName(String val);

  String getShortName();

  void setShortName(String val);

  void remNote(Note val);

  List<? extends Note> getAllNote();

  void addNote(Note val);

  SequenceAssembly getAssembly();

  void setAssembly(SequenceAssembly val);
}
