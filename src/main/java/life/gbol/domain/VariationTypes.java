package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum VariationTypes implements EnumClass {
  SSR("http://gbol.life/0.1/SSR",new VariationTypes[]{}),

  RFLP("http://gbol.life/0.1/RFLP",new VariationTypes[]{}),

  InDel("http://gbol.life/0.1/InDel",new VariationTypes[]{}),

  SNP("http://gbol.life/0.1/SNP",new VariationTypes[]{});

  private VariationTypes[] parents;

  private String iri;

  private VariationTypes(String iri, VariationTypes[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static VariationTypes make(String iri) {
    for(VariationTypes item : VariationTypes.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
