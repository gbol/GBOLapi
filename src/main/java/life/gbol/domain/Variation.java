package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Variation extends VariationFeature {
  void remGenotype(VariantGenotype val);

  List<? extends VariantGenotype> getAllGenotype();

  void addGenotype(VariantGenotype val);

  Integer getHomozygousSamples();

  void setHomozygousSamples(Integer val);

  String getValidated();

  void setValidated(String val);

  Position getEnd();

  void setEnd(Position val);

  String getReadDepth();

  void setReadDepth(String val);

  Integer getNumberOfSamples();

  void setNumberOfSamples(Integer val);

  String getMapQuality();

  void setMapQuality(String val);

  String getAlleleFreq();

  void setAlleleFreq(String val);

  String getReferenceAllele();

  void setReferenceAllele(String val);

  String getAncestralAllele();

  void setAncestralAllele(String val);

  String getQuality();

  void setQuality(String val);

  Integer getNumberNotCalled();

  void setNumberNotCalled(Integer val);

  Integer getAlleleNumber();

  void setAlleleNumber(Integer val);

  Integer getAlleleCount();

  void setAlleleCount(Integer val);

  VariationTypes getVarType();

  void setVarType(VariationTypes val);

  String getStrandBias();

  void setStrandBias(String val);

  Integer getHeterozygousSamples();

  void setHeterozygousSamples(Integer val);

  void remAlternate(String val);

  List<? extends String> getAllAlternate();

  void addAlternate(String val);

  String getDbSNP();

  void setDbSNP(String val);

  String getCIGAR();

  void setCIGAR(String val);

  String getID();

  void setID(String val);

  String getFilter();

  void setFilter(String val);

  Integer getWildtypeSamples();

  void setWildtypeSamples(Integer val);

  String getBaseQuality();

  void setBaseQuality(String val);
}
