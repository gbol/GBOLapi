package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ASVSet extends SequenceSet {
  String getMasterASVId();

  void setMasterASVId(String val);

  Float getRatio();

  void setRatio(Float val);

  Integer getClusteredReadCount();

  void setClusteredReadCount(Integer val);

  ASVSequence getReverseASV();

  void setReverseASV(ASVSequence val);

  ASVSequence getForwardASV();

  void setForwardASV(ASVSequence val);

  Taxon getAssignedTaxon();

  void setAssignedTaxon(Taxon val);

  Integer getReadCount();

  void setReadCount(Integer val);

  void remAsvAssignment(ASVAssignment val);

  List<? extends ASVAssignment> getAllAsvAssignment();

  void addAsvAssignment(ASVAssignment val);

  Integer getTaxonHitCount();

  void setTaxonHitCount(Integer val);

  void remMismatchLevelCount(Integer val);

  List<? extends Integer> getAllMismatchLevelCount();

  void addMismatchLevelCount(Integer val);

  Integer getUsedTaxonLevel();

  void setUsedTaxonLevel(Integer val);

  Integer getMismatchCount();

  void setMismatchCount(Integer val);

  TaxonAssignmentType getTaxonType();

  void setTaxonType(TaxonAssignmentType val);
}
