package life.gbol.domain;

import java.lang.Double;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface QTL extends Feature {
  String getComment();

  void setComment(String val);

  QTLMap getQtlMap();

  void setQtlMap(QTLMap val);

  void remTrait(String val);

  List<? extends String> getAllTrait();

  void addTrait(String val);

  void remAssociatedFeature(Feature val);

  List<? extends Feature> getAllAssociatedFeature();

  void addAssociatedFeature(Feature val);

  String getSymbol();

  void setSymbol(String val);

  Double getRelativeStart();

  void setRelativeStart(Double val);

  Double getRelativeEnd();

  void setRelativeEnd(Double val);

  String getAccession();

  void setAccession(String val);

  void remParent(Organism val);

  List<? extends Organism> getAllParent();

  void addParent(Organism val);

  String getTraitName();

  void setTraitName(String val);

  void remTerm(String val);

  List<? extends String> getAllTerm();

  void addTerm(String val);

  QTLTypes getType();

  void setType(QTLTypes val);

  String getLinkageGroup();

  void setLinkageGroup(String val);
}
