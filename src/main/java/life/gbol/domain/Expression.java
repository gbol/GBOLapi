package life.gbol.domain;

import java.lang.Double;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Expression extends OWLThing {
  Experiment getExperiment();

  void setExperiment(Experiment val);

  ExpressionType getExpressionType();

  void setExpressionType(ExpressionType val);

  ProvenanceApplication getAnnotation();

  void setAnnotation(ProvenanceApplication val);

  Double getValue();

  void setValue(Double val);
}
