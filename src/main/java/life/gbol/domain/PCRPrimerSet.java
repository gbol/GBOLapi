package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface PCRPrimerSet extends SequenceSet {
  PCRPrimer getReversePrimer();

  void setReversePrimer(PCRPrimer val);

  PCRPrimer getForwardPrimer();

  void setForwardPrimer(PCRPrimer val);
}
