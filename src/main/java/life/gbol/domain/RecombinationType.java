package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum RecombinationType implements EnumClass {
  Meiotic("http://gbol.life/0.1/Meiotic",new RecombinationType[]{}),

  RecombinationTypeOther("http://gbol.life/0.1/RecombinationTypeOther",new RecombinationType[]{}),

  ChromosomeBreakpoint("http://gbol.life/0.1/ChromosomeBreakpoint",new RecombinationType[]{}),

  NonallelicHomologous("http://gbol.life/0.1/NonallelicHomologous",new RecombinationType[]{}),

  Mitotic("http://gbol.life/0.1/Mitotic",new RecombinationType[]{});

  private RecombinationType[] parents;

  private String iri;

  private RecombinationType(String iri, RecombinationType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static RecombinationType make(String iri) {
    for(RecombinationType item : RecombinationType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
