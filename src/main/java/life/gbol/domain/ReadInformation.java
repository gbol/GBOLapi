package life.gbol.domain;

import java.lang.Long;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ReadInformation extends OWLThing {
  String getCenter();

  void setCenter(String val);

  Long getBases();

  void setBases(Long val);

  void remFile(LocalSequenceFile val);

  List<? extends LocalSequenceFile> getAllFile();

  void addFile(LocalSequenceFile val);

  Long getReadLength();

  void setReadLength(Long val);

  String getMachine();

  void setMachine(String val);

  String getAdapter();

  void setAdapter(String val);
}
