package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ReplicationOrigin extends BiologicalRecognizedRegion {
  StrandPosition getReplicationDirection();

  void setReplicationDirection(StrandPosition val);
}
