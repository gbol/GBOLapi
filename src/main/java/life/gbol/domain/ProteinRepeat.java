package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ProteinRepeat extends ProteinFeature {
  String getRptUnitSeq();

  void setRptUnitSeq(String val);

  Region getRptUnitRange();

  void setRptUnitRange(Region val);
}
