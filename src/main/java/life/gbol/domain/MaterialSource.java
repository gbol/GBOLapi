package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MaterialSource extends OWLThing {
  String getSampleId();

  void setSampleId(String val);

  CollectionSampleType getCollectionSampleType();

  void setCollectionSampleType(CollectionSampleType val);

  String getCollectionCode();

  void setCollectionCode(String val);

  String getInstitutionCode();

  void setInstitutionCode(String val);
}
