package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum SequencingPlatform implements EnumClass {
  illumina("http://gbol.life/0.1/illumina",new SequencingPlatform[]{}),

  hiseq("http://gbol.life/0.1/hiseq",new SequencingPlatform[]{illumina}),

  hiseqx("http://gbol.life/0.1/hiseqx",new SequencingPlatform[]{hiseq}),

  hiseqx_five("http://gbol.life/0.1/hiseqx_five",new SequencingPlatform[]{hiseqx}),

  nextseq("http://gbol.life/0.1/nextseq",new SequencingPlatform[]{illumina}),

  nextseq500("http://gbol.life/0.1/nextseq500",new SequencingPlatform[]{nextseq}),

  x454("http://gbol.life/0.1/x454",new SequencingPlatform[]{}),

  SOLID("http://gbol.life/0.1/SOLID",new SequencingPlatform[]{}),

  hiseqx_ten("http://gbol.life/0.1/hiseqx_ten",new SequencingPlatform[]{hiseqx}),

  miseq("http://gbol.life/0.1/miseq",new SequencingPlatform[]{illumina}),

  novaseq("http://gbol.life/0.1/novaseq",new SequencingPlatform[]{illumina}),

  novaseq600("http://gbol.life/0.1/novaseq600",new SequencingPlatform[]{novaseq}),

  hiseq4000("http://gbol.life/0.1/hiseq4000",new SequencingPlatform[]{hiseq}),

  miseqdx("http://gbol.life/0.1/miseqdx",new SequencingPlatform[]{miseq}),

  hiseq2000("http://gbol.life/0.1/hiseq2000",new SequencingPlatform[]{hiseq}),

  hiseq3000("http://gbol.life/0.1/hiseq3000",new SequencingPlatform[]{hiseq}),

  nextseq550("http://gbol.life/0.1/nextseq550",new SequencingPlatform[]{nextseq}),

  Nanopore("http://gbol.life/0.1/Nanopore",new SequencingPlatform[]{}),

  miniseq("http://gbol.life/0.1/miniseq",new SequencingPlatform[]{illumina}),

  miseqfgx("http://gbol.life/0.1/miseqfgx",new SequencingPlatform[]{miseq});

  private SequencingPlatform[] parents;

  private String iri;

  private SequencingPlatform(String iri, SequencingPlatform[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static SequencingPlatform make(String iri) {
    for(SequencingPlatform item : SequencingPlatform.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
