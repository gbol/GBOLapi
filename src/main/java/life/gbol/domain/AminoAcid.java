package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;
/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum AminoAcid implements EnumClass {
  Asn("http://gbol.life/0.1/Asn",new AminoAcid[]{}),

  Hyl("http://gbol.life/0.1/Hyl",new AminoAcid[]{}),

  Asp("http://gbol.life/0.1/Asp",new AminoAcid[]{}),

  Thr("http://gbol.life/0.1/Thr",new AminoAcid[]{}),

  AminoAcidOther("http://gbol.life/0.1/AminoAcidOther",new AminoAcid[]{}),

  Pyl("http://gbol.life/0.1/Pyl",new AminoAcid[]{}),

  Asx("http://gbol.life/0.1/Asx",new AminoAcid[]{}),

  Lys("http://gbol.life/0.1/Lys",new AminoAcid[]{}),

  Xle("http://gbol.life/0.1/Xle",new AminoAcid[]{}),

  Orn("http://gbol.life/0.1/Orn",new AminoAcid[]{}),

  Phe("http://gbol.life/0.1/Phe",new AminoAcid[]{}),

  Abu("http://gbol.life/0.1/Abu",new AminoAcid[]{}),

  Ile("http://gbol.life/0.1/Ile",new AminoAcid[]{}),

  bAla("http://gbol.life/0.1/bAla",new AminoAcid[]{}),

  Val("http://gbol.life/0.1/Val",new AminoAcid[]{}),

  Leu("http://gbol.life/0.1/Leu",new AminoAcid[]{}),

  Ahe("http://gbol.life/0.1/Ahe",new AminoAcid[]{}),

  Apm("http://gbol.life/0.1/Apm",new AminoAcid[]{}),

  MeGly("http://gbol.life/0.1/MeGly",new AminoAcid[]{}),

  Ide("http://gbol.life/0.1/Ide",new AminoAcid[]{}),

  Gln("http://gbol.life/0.1/Gln",new AminoAcid[]{}),

  EtAsn("http://gbol.life/0.1/EtAsn",new AminoAcid[]{}),

  ThreeHyp("http://gbol.life/0.1/ThreeHyp",new AminoAcid[]{}),

  Des("http://gbol.life/0.1/Des",new AminoAcid[]{}),

  His("http://gbol.life/0.1/His",new AminoAcid[]{}),

  Acp("http://gbol.life/0.1/Acp",new AminoAcid[]{}),

  Tyr("http://gbol.life/0.1/Tyr",new AminoAcid[]{}),

  Xaa("http://gbol.life/0.1/Xaa",new AminoAcid[]{}),

  Nva("http://gbol.life/0.1/Nva",new AminoAcid[]{}),

  Ala("http://gbol.life/0.1/Ala",new AminoAcid[]{}),

  TERM("http://gbol.life/0.1/TERM",new AminoAcid[]{}),

  MeIle("http://gbol.life/0.1/MeIle",new AminoAcid[]{}),

  Cys("http://gbol.life/0.1/Cys",new AminoAcid[]{}),

  MeVal("http://gbol.life/0.1/MeVal",new AminoAcid[]{}),

  Aad("http://gbol.life/0.1/Aad",new AminoAcid[]{}),

  Glu("http://gbol.life/0.1/Glu",new AminoAcid[]{}),

  Trp("http://gbol.life/0.1/Trp",new AminoAcid[]{}),

  EtGly("http://gbol.life/0.1/EtGly",new AminoAcid[]{}),

  Pro("http://gbol.life/0.1/Pro",new AminoAcid[]{}),

  aHyl("http://gbol.life/0.1/aHyl",new AminoAcid[]{}),

  Dbu("http://gbol.life/0.1/Dbu",new AminoAcid[]{}),

  Aib("http://gbol.life/0.1/Aib",new AminoAcid[]{}),

  MeLys("http://gbol.life/0.1/MeLys",new AminoAcid[]{}),

  Gly("http://gbol.life/0.1/Gly",new AminoAcid[]{}),

  Glx("http://gbol.life/0.1/Glx",new AminoAcid[]{}),

  Ser("http://gbol.life/0.1/Ser",new AminoAcid[]{}),

  Dpm("http://gbol.life/0.1/Dpm",new AminoAcid[]{}),

  Met("http://gbol.life/0.1/Met",new AminoAcid[]{}),

  Nle("http://gbol.life/0.1/Nle",new AminoAcid[]{}),

  Dpr("http://gbol.life/0.1/Dpr",new AminoAcid[]{}),

  bAad("http://gbol.life/0.1/bAad",new AminoAcid[]{}),

  FourAbu("http://gbol.life/0.1/FourAbu",new AminoAcid[]{}),

  aIle("http://gbol.life/0.1/aIle",new AminoAcid[]{}),

  Sec("http://gbol.life/0.1/Sec",new AminoAcid[]{}),

  FourHyp("http://gbol.life/0.1/FourHyp",new AminoAcid[]{}),

  Arg("http://gbol.life/0.1/Arg",new AminoAcid[]{}),

  bAib("http://gbol.life/0.1/bAib",new AminoAcid[]{});

  private AminoAcid[] parents;

  private String iri;

  private AminoAcid(String iri, AminoAcid[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static AminoAcid make(String iri) {
    for(AminoAcid item : AminoAcid.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
