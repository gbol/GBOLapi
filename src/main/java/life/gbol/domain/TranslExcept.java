package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface TranslExcept extends OWLThing {
  Region getExceptRegion();

  void setExceptRegion(Region val);

  AminoAcid getAminoAcid();

  void setAminoAcid(AminoAcid val);
}
