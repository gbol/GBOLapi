package life.gbol.domain;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface MiscRecomb extends BiologicalRecognizedRegion {
  RecombinationType getRecombinationType();

  void setRecombinationType(RecombinationType val);
}
