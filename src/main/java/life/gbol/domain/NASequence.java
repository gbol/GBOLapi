package life.gbol.domain;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface NASequence extends Sequence {
  Topology getTopology();

  void setTopology(Topology val);

  void remClone(String val);

  List<? extends String> getAllClone();

  void addClone(String val);

  void remExpression(Expression val);

  List<? extends Expression> getAllExpression();

  void addExpression(Expression val);

  Boolean getRearranged();

  void setRearranged(Boolean val);

  Integer getTranslTable();

  void setTranslTable(Integer val);

  Boolean getMacronuclear();

  void setMacronuclear(Boolean val);

  void remFeature(NAFeature val);

  List<? extends NAFeature> getAllFeature();

  void addFeature(NAFeature val);

  NASequence getIntegratedInto();

  void setIntegratedInto(NASequence val);

  Boolean getProviral();

  void setProviral(Boolean val);

  String getCloneLib();

  void setCloneLib(String val);
}
