package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ExpressionType implements EnumClass {
  Counts("http://gbol.life/0.1/Counts",new ExpressionType[]{}),

  RawCounts("http://gbol.life/0.1/RawCounts",new ExpressionType[]{Counts}),

  Normalised("http://gbol.life/0.1/Normalised",new ExpressionType[]{}),

  TPM("http://gbol.life/0.1/TPM",new ExpressionType[]{Normalised}),

  FPKM("http://gbol.life/0.1/FPKM",new ExpressionType[]{Normalised}),

  EstimatedCounts("http://gbol.life/0.1/EstimatedCounts",new ExpressionType[]{Counts}),

  ExpectedCounts("http://gbol.life/0.1/ExpectedCounts",new ExpressionType[]{Counts}),

  RPKM("http://gbol.life/0.1/RPKM",new ExpressionType[]{Normalised});

  private ExpressionType[] parents;

  private String iri;

  private ExpressionType(String iri, ExpressionType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ExpressionType make(String iri) {
    for(ExpressionType item : ExpressionType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
