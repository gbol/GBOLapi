package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public enum ReadOrientation implements EnumClass {
  RF("http://gbol.life/0.1/RF",new ReadOrientation[]{}),

  FF("http://gbol.life/0.1/FF",new ReadOrientation[]{}),

  RR("http://gbol.life/0.1/RR",new ReadOrientation[]{}),

  FR("http://gbol.life/0.1/FR",new ReadOrientation[]{});

  private ReadOrientation[] parents;

  private String iri;

  private ReadOrientation(String iri, ReadOrientation[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static ReadOrientation make(String iri) {
    for(ReadOrientation item : ReadOrientation.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
