package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface SequencePart extends OWLThing {
}
