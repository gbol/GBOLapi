package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface RejectedAsChimera extends ASVSet {
  Integer getPReverseId();

  void setPReverseId(Integer val);

  Integer getPForwardId();

  void setPForwardId(Integer val);

  Float getPForwardRatio();

  void setPForwardRatio(Float val);

  Float getPReverseRatio();

  void setPReverseRatio(Float val);
}
