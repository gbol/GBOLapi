package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface ASVAssignment extends ProvenanceClassification {
  Float getRatio();

  void setRatio(Float val);

  Taxon getTaxon();

  void setTaxon(Taxon val);

  TaxonAssignmentType getType();

  void setType(TaxonAssignmentType val);

  Integer getNumberHits();

  void setNumberHits(Integer val);

  Integer getLevelCount();

  void setLevelCount(Integer val);
}
