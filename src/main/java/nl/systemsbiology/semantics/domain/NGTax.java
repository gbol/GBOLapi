package nl.systemsbiology.semantics.domain;

import java.lang.Boolean;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.LocalSequenceFile;
import life.gbol.domain.ProvenanceClassification;

/**
 * Code generated from http://semantics.systemsbiology.nl/0.1/ ontology
 */
public interface NGTax extends ProvenanceClassification {
  String getGeneratedBy();

  void setGeneratedBy(String val);

  String getRefdb();

  void setRefdb(String val);

  Integer getMaxChemeraDistR();

  void setMaxChemeraDistR(Integer val);

  String getDate();

  void setDate(String val);

  Integer getMaxChemeraDistF();

  void setMaxChemeraDistF(Integer val);

  Integer getErrorCorr();

  void setErrorCorr(Integer val);

  Integer getRja();

  void setRja(Integer val);

  String getFormat();

  void setFormat(String val);

  String getFormatURL();

  void setFormatURL(String val);

  Integer getMaxClusteringMismatchCount();

  void setMaxClusteringMismatchCount(Integer val);

  String getMatrixType();

  void setMatrixType(String val);

  Integer getRReadLength();

  void setRReadLength(Integer val);

  String getLogFile();

  void setLogFile(String val);

  Float getClassifyRatio();

  void setClassifyRatio(Float val);

  Integer getIdentity92MismatchCount();

  void setIdentity92MismatchCount(Integer val);

  String getHeaderType();

  void setHeaderType(String val);

  Integer getMinOTUSizeT();

  void setMinOTUSizeT(Integer val);

  String getEmail();

  void setEmail(String val);

  Boolean getMarkIfMoreThen1();

  void setMarkIfMoreThen1(Boolean val);

  Integer getIdentity95MismatchCount();

  void setIdentity95MismatchCount(Integer val);

  String getMatrixElementType();

  void setMatrixElementType(String val);

  Float getIdentLvl();

  void setIdentLvl(Float val);

  Integer getIdentity97MismatchCount();

  void setIdentity97MismatchCount(Integer val);

  Float getMinPerT();

  void setMinPerT(Float val);

  String getVersion();

  void setVersion(String val);

  String getMapFile();

  void setMapFile(String val);

  Integer getRPrimerLength();

  void setRPrimerLength(Integer val);

  String getCommit();

  void setCommit(String val);

  Integer getFReadLength();

  void setFReadLength(Integer val);

  Integer getIdentity90MismatchCount();

  void setIdentity90MismatchCount(Integer val);

  String getBiomFile();

  void setBiomFile(String val);

  String getProject();

  void setProject(String val);

  void remFastQSet(LocalSequenceFile val);

  List<? extends LocalSequenceFile> getAllFastQSet();

  void addFastQSet(LocalSequenceFile val);

  Integer getIdentity85MismatchCount();

  void setIdentity85MismatchCount(Integer val);

  Integer getFPrimerLength();

  void setFPrimerLength(Integer val);

  String getFolder();

  void setFolder(String val);

  Boolean getMismatchDB();

  void setMismatchDB(Boolean val);

  Boolean getShanon();

  void setShanon(Boolean val);

  String getTtlFile();

  void setTtlFile(String val);

  String getId();

  void setId(String val);

  Float getChimeraRatio();

  void setChimeraRatio(Float val);

  String getSubFragment();

  void setSubFragment(String val);
}
