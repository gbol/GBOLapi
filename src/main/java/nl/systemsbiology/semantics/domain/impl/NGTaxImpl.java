package nl.systemsbiology.semantics.domain.impl;

import java.lang.Boolean;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.LocalSequenceFile;
import life.gbol.domain.impl.ProvenanceClassificationImpl;
import nl.systemsbiology.semantics.domain.NGTax;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://semantics.systemsbiology.nl/0.1/ ontology
 */
public class NGTaxImpl extends ProvenanceClassificationImpl implements NGTax {
  public static final String TypeIRI = "http://semantics.systemsbiology.nl/0.1/NGTax";

  protected NGTaxImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static NGTax make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new NGTaxImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,NGTax.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,NGTax.class,false);
          if(toRet == null) {
            toRet = new NGTaxImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof NGTax)) {
          throw new RuntimeException("Instance of nl.systemsbiology.semantics.domain.impl.NGTaxImpl expected");
        }
      }
      return (NGTax)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/generatedBy");
    this.checkCardMin1("http://gbol.life/0.1/maxChemeraDistR");
    this.checkCardMin1("http://gbol.life/0.1/date");
    this.checkCardMin1("http://gbol.life/0.1/maxChemeraDistF");
    this.checkCardMin1("http://gbol.life/0.1/errorCorr");
    this.checkCardMin1("http://gbol.life/0.1/format");
    this.checkCardMin1("http://gbol.life/0.1/formatURL");
    this.checkCardMin1("http://gbol.life/0.1/maxClusteringMismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/matrixType");
    this.checkCardMin1("http://gbol.life/0.1/rReadLength");
    this.checkCardMin1("http://gbol.life/0.1/classifyRatio");
    this.checkCardMin1("http://gbol.life/0.1/identity92MismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/headerType");
    this.checkCardMin1("http://gbol.life/0.1/minOTUSizeT");
    this.checkCardMin1("http://gbol.life/0.1/markIfMoreThen1");
    this.checkCardMin1("http://gbol.life/0.1/identity95MismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/matrixElementType");
    this.checkCardMin1("http://gbol.life/0.1/identLvl");
    this.checkCardMin1("http://gbol.life/0.1/identity97MismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/minPerT");
    this.checkCardMin1("http://gbol.life/0.1/version");
    this.checkCardMin1("http://gbol.life/0.1/mapFile");
    this.checkCardMin1("http://gbol.life/0.1/rPrimerLength");
    this.checkCardMin1("http://gbol.life/0.1/fReadLength");
    this.checkCardMin1("http://gbol.life/0.1/identity90MismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/biomFile");
    this.checkCardMin1("http://gbol.life/0.1/fastQSet");
    this.checkCardMin1("http://gbol.life/0.1/identity85MismatchCount");
    this.checkCardMin1("http://gbol.life/0.1/fPrimerLength");
    this.checkCardMin1("http://gbol.life/0.1/id");
    this.checkCardMin1("http://gbol.life/0.1/chimeraRatio");
  }

  public String getGeneratedBy() {
    return this.getStringLit("http://gbol.life/0.1/generatedBy",false);
  }

  public void setGeneratedBy(String val) {
    this.setStringLit("http://gbol.life/0.1/generatedBy",val);
  }

  public String getRefdb() {
    return this.getStringLit("http://gbol.life/0.1/refdb",true);
  }

  public void setRefdb(String val) {
    this.setStringLit("http://gbol.life/0.1/refdb",val);
  }

  public Integer getMaxChemeraDistR() {
    return this.getIntegerLit("http://gbol.life/0.1/maxChemeraDistR",false);
  }

  public void setMaxChemeraDistR(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/maxChemeraDistR",val);
  }

  public String getDate() {
    return this.getStringLit("http://gbol.life/0.1/date",false);
  }

  public void setDate(String val) {
    this.setStringLit("http://gbol.life/0.1/date",val);
  }

  public Integer getMaxChemeraDistF() {
    return this.getIntegerLit("http://gbol.life/0.1/maxChemeraDistF",false);
  }

  public void setMaxChemeraDistF(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/maxChemeraDistF",val);
  }

  public Integer getErrorCorr() {
    return this.getIntegerLit("http://gbol.life/0.1/errorCorr",false);
  }

  public void setErrorCorr(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/errorCorr",val);
  }

  public Integer getRja() {
    return this.getIntegerLit("http://gbol.life/0.1/rja",true);
  }

  public void setRja(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/rja",val);
  }

  public String getFormat() {
    return this.getStringLit("http://gbol.life/0.1/format",false);
  }

  public void setFormat(String val) {
    this.setStringLit("http://gbol.life/0.1/format",val);
  }

  public String getFormatURL() {
    return this.getStringLit("http://gbol.life/0.1/formatURL",false);
  }

  public void setFormatURL(String val) {
    this.setStringLit("http://gbol.life/0.1/formatURL",val);
  }

  public Integer getMaxClusteringMismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/maxClusteringMismatchCount",false);
  }

  public void setMaxClusteringMismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/maxClusteringMismatchCount",val);
  }

  public String getMatrixType() {
    return this.getStringLit("http://gbol.life/0.1/matrixType",false);
  }

  public void setMatrixType(String val) {
    this.setStringLit("http://gbol.life/0.1/matrixType",val);
  }

  public Integer getRReadLength() {
    return this.getIntegerLit("http://gbol.life/0.1/rReadLength",false);
  }

  public void setRReadLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/rReadLength",val);
  }

  public String getLogFile() {
    return this.getStringLit("http://gbol.life/0.1/logFile",true);
  }

  public void setLogFile(String val) {
    this.setStringLit("http://gbol.life/0.1/logFile",val);
  }

  public Float getClassifyRatio() {
    return this.getFloatLit("http://gbol.life/0.1/classifyRatio",false);
  }

  public void setClassifyRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/classifyRatio",val);
  }

  public Integer getIdentity92MismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/identity92MismatchCount",false);
  }

  public void setIdentity92MismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/identity92MismatchCount",val);
  }

  public String getHeaderType() {
    return this.getStringLit("http://gbol.life/0.1/headerType",false);
  }

  public void setHeaderType(String val) {
    this.setStringLit("http://gbol.life/0.1/headerType",val);
  }

  public Integer getMinOTUSizeT() {
    return this.getIntegerLit("http://gbol.life/0.1/minOTUSizeT",false);
  }

  public void setMinOTUSizeT(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/minOTUSizeT",val);
  }

  public String getEmail() {
    return this.getStringLit("http://gbol.life/0.1/email",true);
  }

  public void setEmail(String val) {
    this.setStringLit("http://gbol.life/0.1/email",val);
  }

  public Boolean getMarkIfMoreThen1() {
    return this.getBooleanLit("http://gbol.life/0.1/markIfMoreThen1",false);
  }

  public void setMarkIfMoreThen1(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/markIfMoreThen1",val);
  }

  public Integer getIdentity95MismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/identity95MismatchCount",false);
  }

  public void setIdentity95MismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/identity95MismatchCount",val);
  }

  public String getMatrixElementType() {
    return this.getStringLit("http://gbol.life/0.1/matrixElementType",false);
  }

  public void setMatrixElementType(String val) {
    this.setStringLit("http://gbol.life/0.1/matrixElementType",val);
  }

  public Float getIdentLvl() {
    return this.getFloatLit("http://gbol.life/0.1/identLvl",false);
  }

  public void setIdentLvl(Float val) {
    this.setFloatLit("http://gbol.life/0.1/identLvl",val);
  }

  public Integer getIdentity97MismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/identity97MismatchCount",false);
  }

  public void setIdentity97MismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/identity97MismatchCount",val);
  }

  public Float getMinPerT() {
    return this.getFloatLit("http://gbol.life/0.1/minPerT",false);
  }

  public void setMinPerT(Float val) {
    this.setFloatLit("http://gbol.life/0.1/minPerT",val);
  }

  public String getVersion() {
    return this.getStringLit("http://gbol.life/0.1/version",false);
  }

  public void setVersion(String val) {
    this.setStringLit("http://gbol.life/0.1/version",val);
  }

  public String getMapFile() {
    return this.getStringLit("http://gbol.life/0.1/mapFile",false);
  }

  public void setMapFile(String val) {
    this.setStringLit("http://gbol.life/0.1/mapFile",val);
  }

  public Integer getRPrimerLength() {
    return this.getIntegerLit("http://gbol.life/0.1/rPrimerLength",false);
  }

  public void setRPrimerLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/rPrimerLength",val);
  }

  public String getCommit() {
    return this.getStringLit("http://gbol.life/0.1/commit",true);
  }

  public void setCommit(String val) {
    this.setStringLit("http://gbol.life/0.1/commit",val);
  }

  public Integer getFReadLength() {
    return this.getIntegerLit("http://gbol.life/0.1/fReadLength",false);
  }

  public void setFReadLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/fReadLength",val);
  }

  public Integer getIdentity90MismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/identity90MismatchCount",false);
  }

  public void setIdentity90MismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/identity90MismatchCount",val);
  }

  public String getBiomFile() {
    return this.getStringLit("http://gbol.life/0.1/biomFile",false);
  }

  public void setBiomFile(String val) {
    this.setStringLit("http://gbol.life/0.1/biomFile",val);
  }

  public String getProject() {
    return this.getStringLit("http://gbol.life/0.1/project",true);
  }

  public void setProject(String val) {
    this.setStringLit("http://gbol.life/0.1/project",val);
  }

  public void remFastQSet(LocalSequenceFile val) {
    this.remRef("http://gbol.life/0.1/fastQSet",val,false);
  }

  public List<? extends LocalSequenceFile> getAllFastQSet() {
    return this.getRefSet("http://gbol.life/0.1/fastQSet",false,LocalSequenceFile.class);
  }

  public void addFastQSet(LocalSequenceFile val) {
    this.addRef("http://gbol.life/0.1/fastQSet",val);
  }

  public Integer getIdentity85MismatchCount() {
    return this.getIntegerLit("http://gbol.life/0.1/identity85MismatchCount",false);
  }

  public void setIdentity85MismatchCount(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/identity85MismatchCount",val);
  }

  public Integer getFPrimerLength() {
    return this.getIntegerLit("http://gbol.life/0.1/fPrimerLength",false);
  }

  public void setFPrimerLength(Integer val) {
    this.setIntegerLit("http://gbol.life/0.1/fPrimerLength",val);
  }

  public String getFolder() {
    return this.getStringLit("http://gbol.life/0.1/folder",true);
  }

  public void setFolder(String val) {
    this.setStringLit("http://gbol.life/0.1/folder",val);
  }

  public Boolean getMismatchDB() {
    return this.getBooleanLit("http://gbol.life/0.1/mismatchDB",true);
  }

  public void setMismatchDB(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/mismatchDB",val);
  }

  public Boolean getShanon() {
    return this.getBooleanLit("http://gbol.life/0.1/shanon",true);
  }

  public void setShanon(Boolean val) {
    this.setBooleanLit("http://gbol.life/0.1/shanon",val);
  }

  public String getTtlFile() {
    return this.getStringLit("http://gbol.life/0.1/ttlFile",true);
  }

  public void setTtlFile(String val) {
    this.setStringLit("http://gbol.life/0.1/ttlFile",val);
  }

  public String getId() {
    return this.getStringLit("http://gbol.life/0.1/id",false);
  }

  public void setId(String val) {
    this.setStringLit("http://gbol.life/0.1/id",val);
  }

  public Float getChimeraRatio() {
    return this.getFloatLit("http://gbol.life/0.1/chimeraRatio",false);
  }

  public void setChimeraRatio(Float val) {
    this.setFloatLit("http://gbol.life/0.1/chimeraRatio",val);
  }

  public String getSubFragment() {
    return this.getStringLit("http://gbol.life/0.1/subFragment",true);
  }

  public void setSubFragment(String val) {
    this.setStringLit("http://gbol.life/0.1/subFragment",val);
  }
}
