package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Person;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class PersonImpl extends AgentImpl implements Person {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Person";

  protected PersonImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Person make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PersonImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Person.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Person.class,false);
          if(toRet == null) {
            toRet = new PersonImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Person)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.PersonImpl expected");
        }
      }
      return (Person)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
