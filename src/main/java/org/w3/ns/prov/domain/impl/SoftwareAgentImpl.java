package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.SoftwareAgent;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class SoftwareAgentImpl extends AgentImpl implements SoftwareAgent {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#SoftwareAgent";

  protected SoftwareAgentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SoftwareAgent make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SoftwareAgentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SoftwareAgent.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SoftwareAgent.class,false);
          if(toRet == null) {
            toRet = new SoftwareAgentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SoftwareAgent)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.SoftwareAgentImpl expected");
        }
      }
      return (SoftwareAgent)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
