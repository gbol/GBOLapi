package org.w3.ns.prov.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class EntityImpl extends OWLThingImpl implements Entity {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Entity";

  protected EntityImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Entity make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new EntityImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Entity.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Entity.class,false);
          if(toRet == null) {
            toRet = new EntityImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Entity)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.EntityImpl expected");
        }
      }
      return (Entity)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }

  public void remWasDerivedFrom(Entity val) {
    this.remRef("http://www.w3.org/ns/prov#wasDerivedFrom",val,true);
  }

  public List<? extends Entity> getAllWasDerivedFrom() {
    return this.getRefSet("http://www.w3.org/ns/prov#wasDerivedFrom",true,Entity.class);
  }

  public void addWasDerivedFrom(Entity val) {
    this.addRef("http://www.w3.org/ns/prov#wasDerivedFrom",val);
  }
}
