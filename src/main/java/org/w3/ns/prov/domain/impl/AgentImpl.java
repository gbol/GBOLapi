package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.ns.prov.domain.Agent;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class AgentImpl extends OWLThingImpl implements Agent {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Agent";

  protected AgentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Agent make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AgentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Agent.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Agent.class,false);
          if(toRet == null) {
            toRet = new AgentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Agent)) {
          throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.AgentImpl expected");
        }
      }
      return (Agent)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
