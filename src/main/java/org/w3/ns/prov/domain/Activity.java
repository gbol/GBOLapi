package org.w3.ns.prov.domain;

import java.time.LocalDateTime;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Activity extends OWLThing {
  LocalDateTime getStartedAtTime();

  void setStartedAtTime(LocalDateTime val);

  Agent getWasAssociatedWith();

  void setWasAssociatedWith(Agent val);

  LocalDateTime getEndedAtTime();

  void setEndedAtTime(LocalDateTime val);

  void remUsed(Entity val);

  List<? extends Entity> getAllUsed();

  void addUsed(Entity val);

  void remGenerated(Entity val);

  List<? extends Entity> getAllGenerated();

  void addGenerated(Entity val);

  Activity getWasInformedBy();

  void setWasInformedBy(Activity val);
}
