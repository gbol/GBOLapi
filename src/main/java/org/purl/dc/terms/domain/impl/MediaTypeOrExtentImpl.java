package org.purl.dc.terms.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.purl.dc.terms.domain.MediaTypeOrExtent;

/**
 * Code generated from http://purl.org/dc/terms/ ontology
 */
public class MediaTypeOrExtentImpl extends OWLThingImpl implements MediaTypeOrExtent {
  public static final String TypeIRI = "http://purl.org/dc/terms/MediaTypeOrExtent";

  protected MediaTypeOrExtentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static MediaTypeOrExtent make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new MediaTypeOrExtentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,MediaTypeOrExtent.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,MediaTypeOrExtent.class,false);
          if(toRet == null) {
            toRet = new MediaTypeOrExtentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof MediaTypeOrExtent)) {
          throw new RuntimeException("Instance of org.purl.dc.terms.domain.impl.MediaTypeOrExtentImpl expected");
        }
      }
      return (MediaTypeOrExtent)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://www.w3.org/2000/01/rdf-schema#label");
  }

  public String getValue() {
    return this.getStringLit("http://www.w3.org/1999/02/22-rdf-syntax-ns#value",true);
  }

  public void setValue(String val) {
    this.setStringLit("http://www.w3.org/1999/02/22-rdf-syntax-ns#value",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",false);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }
}
